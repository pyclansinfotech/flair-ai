from home.models import Activity
import uuid
import datetime
from django.contrib.auth.models import User


class Activitylogger(object):

    @classmethod
    def write(cls, data):
        activity = Activity(id=uuid.uuid1())
        activity.type = data['type']
        activity.activity_timestamp = datetime.datetime.now()
        activity.message = data['message']
        activity.display = data['display']
        activity.styles = data['styles']
        activity.icon = data['icon']
        activity.user = User.objects.get(username=data['username'])
        activity.save()

    @classmethod
    def read(cls):
        records = Activity.objects\
            .values('id', 'type', 'activity_timestamp', 'message', 'user', 'icon')\
            .filter(display=True)\
            .order_by('-activity_timestamp')[:20]
        data = {}
        for line in records:
            data[line['id']] = {}
            data[line['id']]['type'] = line['type']
            data[line['id']]['icon'] = line['icon']
            data[line['id']]['datetime'] = line['activity_timestamp'].strftime("%Y-%m-%d %H:%M:%S")
            data[line['id']]['user'] = User.objects.get(id=line['user']).username
            data[line['id']]['message'] = line['message']
        return data

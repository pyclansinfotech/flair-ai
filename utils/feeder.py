from browser.models import Source


class SourceFeed(object):

    @classmethod
    def totsources(self):
        total = Source.objects.all().count()
        return total

    @classmethod
    def totsourcesinmemory(self):
        total = Source.objects.filter(in_memory=True).count()
        return total

from browser.models import Source, Feature


class SourceSummary(object):

    def __init__(self, id):
        self.id = id
        self.source = Source.objects.get(id=self.id)

    def profile(self):
        source_profile = {}
        source_profile['id'] = self.source.id
        source_profile['table'] = self.source.table
        if self.source.analysed is True:
            source_profile['analysed'] = "checked"
        else:
            source_profile['analysed'] = ""
        if self.source.in_memory is True:
            source_profile['memory'] = "checked"
        else:
            source_profile['memory'] = ""
        source_profile['docfile'] = self.source.docfile
        source_profile['extension'] = self.source.extension
        source_profile['ods'] = self.source.ods_file_location
        source_profile['user'] = self.source.user
        source_profile['features'] = self.features()
        return source_profile


    def features(self):
        retrived_Features = Feature.objects.filter(source_link=self.source)
        feature_profile = {}
        for feature in retrived_Features:
            feature_profile[feature.field_name] = {}
            feature_profile[feature.field_name]['id'] = feature.id
            feature_profile[feature.field_name]['density'] = feature.density
            feature_profile[feature.field_name]['fieldtype'] = feature.fieldtype_link.field_type
            feature_profile[feature.field_name]['datatype'] = feature.datatype_link.data_type
        return feature_profile


class Sources(object):

    @classmethod
    def getall(self):
        ret_sources = Source.objects.all()
        result = {}
        for source in ret_sources:
            result[source.id] = {}
            result[source.id]['table'] = source.table
            result[source.id]['docfile'] = source.docfile
            result[source.id]['ods'] = source.ods_file_location
            if source.analysed is not True:
                result[source.id]['analysed'] = ""
            else:
                result[source.id]['analysed'] = "checked"
            if source.in_memory is not True:
                result[source.id]['memory'] = ""
            else:
                result[source.id]['memory'] = "checked"
            result[source.id]['extension'] = source.extension
            result[source.id]['user'] = source.user.username
        return result



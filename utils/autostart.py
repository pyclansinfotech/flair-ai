from browser.models import Source
from sparkplug.parquet import SparkPlug


class Ignite(object):
    @classmethod
    def all(cls):
        try:
            response = False
            sources = Source.objects.values('id').filter(in_memory=True)
            for source in sources:
                response = SparkPlug.memorypush(source['id'])
            return response
        except Exception as e:
            print(e)
            return False

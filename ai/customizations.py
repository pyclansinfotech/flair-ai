class AdminPanelCustomization(object):

    @classmethod
    def headersettings(self, admin):
        admin.site.site_header = "Flair Artificial Intelligence Platform"
        admin.site.site_title = "Flair AI"
        admin.site.index_title = "Administration"
        return admin

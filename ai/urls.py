"""ai URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import refresh_jwt_token
from rest_framework_jwt.views import verify_jwt_token
from browser.api import BrowserRouterRegistry
from .customizations import AdminPanelCustomization
from home.views import updateAssignedReport, saveConnectionsData,\
    dagScheduler, updateConnectionData, getConnectionData,\
    deleteConnectionData, getConnections, addassign_report,\
    getCreationReportData, getAssignedReportData, deleteCreationReport,\
    deleteAssignedReport, add_report, assign_report_data, save_report,\
    loginuser, home, logoutuser, pysql_execution, python_execution,\
    sql_execution, report_creation, reportLineItem, getReportItemData,\
    updateReportItem, deleteReportItem, createReportItem
from browser.views import file_upload, profiling, finish, sources,\
    deletesource, conversion, runconversion, pushtomemory
from django.contrib.auth.decorators import login_required
from home.api import ActivityRouterRegistry
from sparkplug.cycle import fire
from utils.autostart import Ignite

admin = AdminPanelCustomization.headersettings(admin)
browserrouter = BrowserRouterRegistry.buildrouters()
activityrouter = ActivityRouterRegistry.buildrouters()

# if Ignite.all() is True:
#     print('INFO: Sources added to memory')
# else:
#     print("WARNING: No sources to add to memory. If that's not whats intended please contact support.")

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # url(r'^', include(browserrouter.urls)),
    # url(r'^', include(activityrouter.urls)),
    url(r'^api/v1/query', fire),
    url(r'^browser/', include('browser.urls')),
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^api-token-refresh/', refresh_jwt_token),
    url(r'^api-token-verify/', verify_jwt_token),
    url(r'^home/$', login_required(home), name="home"),
    url(r'^sources/$', login_required(sources), name="sources"),
    url(r'^data/conversion/$', login_required(conversion), name="conversion"),
    url(
        r'^data/runconversion/(?P<id>[^\/]*)/$',
        login_required(runconversion),
        name='runconversion'
    ),
    url(
        r'^data/memory/(?P<id>[^\/]*)/$',
        login_required(pushtomemory),
        name='pushtomemory'
    ),
    url(
        r'^manager/fileupload/',
        login_required(file_upload),
        name='file_upload'
    ),
    url(
        r'^manager/profiling/(?P<id>[^\/]*)/$',
        login_required(profiling),
        name='profiling'
    ),
    url(
        r'^manager/finish/(?P<id>[^\/]*)/$',
        login_required(finish),
        name='finish'
    ),
    url(
        r'^manager/deletesource/(?P<id>[^\/]*)/$',
        login_required(deletesource),
        name='deletesource'
    ),
    url(r'^accounts/login/$', loginuser),
    url(r'^accounts/logout/$', logoutuser),
    url(r'^pysql/$', login_required(pysql_execution), name="pysql_execution"),
    url(
        r'^python_execution/$',
        login_required(python_execution),
        name="python_execution"
    ),
    url(
        r'^sql_execution/$',
        login_required(sql_execution),
        name="sql_execution"
    ),
    url(
        r'^report-creation/$',
        login_required(report_creation),
        name="report_creation"
    ),
    url(
        r'^deleteCreationReport/$',
        login_required(deleteCreationReport),
        name="deleteCreationReport"
    ),
    url(
        r'^getCreationReportData/$',
        login_required(getCreationReportData),
        name="getCreationReportData"
    ),
    url(r'^save_report/$', login_required(save_report), name="save_report"),
    url(
        r'^assign_reports/$',
        login_required(assign_report_data),
        name="assign_report_data"
    ),
    url(r'^add_report/$', login_required(add_report), name="add_report"),
    url(
        r'^deleteAssignedReport/$',
        login_required(deleteAssignedReport),
        name="deleteAssignedReport"
    ),
    url(
        r'^getAssignedReportData/$',
        login_required(getAssignedReportData),
        name="getAssignedReportData"
    ),
    url(
        r'^updateAssignedReport/$',
        login_required(updateAssignedReport),
        name="updateAssignedReport"
    ),
    url(
        r'^addassign_report/$',
        login_required(addassign_report),
        name="addassign_report"
    ),
    url(
        r'^connections/$',
        login_required(getConnections),
        name="getConnections"
    ),
    url(
        r'^saveConnectionsData/$',
        login_required(saveConnectionsData),
        name="saveConnectionsData"
    ),
    url(
        r'^deleteConnectionData/$',
        login_required(deleteConnectionData),
        name="deleteConnectionData"
    ),
    url(
        r'^getConnectionData/$',
        login_required(getConnectionData),
        name="getConnectionData"
    ),
    url(
        r'^updateConnectionData/$',
        login_required(updateConnectionData),
        name="updateConnectionData"
    ),
    url(r'^dagScheduler/$', login_required(dagScheduler), name="dagScheduler"),
    url(
        r'^report-line-item/$',
        login_required(reportLineItem),
        name="reportLineItem"
    ),
    url(
        r'^getReportItemData/$',
        login_required(getReportItemData),
        name="getReportItemData"
    ),
    url(
        r'^updateReportItem/$',
        login_required(updateReportItem),
        name="updateReportItem"
    ),
    url(
        r'^deleteReportItem/$',
        login_required(deleteReportItem),
        name="deleteReportItem"
    ),
    url(
        r'^createReportItem/$',
        login_required(createReportItem),
        name="createReportItem"
    ),
    # API URLS
    url(r'^api/', include('reports_apis.urls')),
    url(
        r'^api-auth/',
        include('rest_framework.urls', namespace='rest_framework')
    ),

]

import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ai.settings")

import django
django.setup()

import requests
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from requests.auth import HTTPDigestAuth
import pdfkit
import jinja2
from jinja2 import Template, Environment, FileSystemLoader
from threading import Thread
import json
from browser.models import Source
from home.models import ReportCreationData, AssignReportsData, Connections,\
    AirflowTasks, ReportLineItem
from cryptography.fernet import Fernet
from django.conf import settings
from django.contrib.staticfiles.storage import staticfiles_storage
from django.contrib.staticfiles.templatetags.staticfiles import static
from datetime import datetime
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import uuid
import numpy as np


plt.rcParams['font.size'] = 15.0
path = settings.BASE_DIR
pdf_dir = os.path.join(path, 'pdf/')
static_path = os.path.join(path, 'webapp', 'static/')
template_path = os.path.join(path, 'webapp', 'templates')
report_charts_img_dir = os.path.join(
    path, 'webapp', 'static',
    'images', 'report_charts/'
)


# func to created thread based process
def async(f):
    def wrapper(*args, **kwargs):
        thr = Thread(target=f, args=args, kwargs=kwargs)
        thr.start()
    return wrapper


def decrypt_pass(password):
    try:
        cipher = session['cipher']
        decoded = DecodeAES(cipher, password)
        return decoded
    except Exception as e:
        print('error while decrypt', e)


# func to fetch report data
def getReportdata(assign_report_id):
    # fetch assign report data
    assign_rep_obj = AssignReportsData.objects.filter(pk=assign_report_id)
    if assign_rep_obj:
        for obj in assign_rep_obj:
            receiver_email = obj.email
        report_creation_obj = ReportCreationData.objects.filter(
            pk=obj.report_creation_data_id
        )
        for obj in report_creation_obj:
            report_id = obj.id
            connection_id = obj.connection_id
            subject = obj.subject
            mail_body = obj.mail_body
            report_name = obj.report_name

        test_list = []
        # fetch report line item
        report_line_item_obj = ReportLineItem.objects.filter(
            report_id=report_id
        )
        for line_item_obj in report_line_item_obj:
            query_name = line_item_obj.query_name
            fields = line_item_obj.fields
            group_by = line_item_obj.group_by
            order_by = line_item_obj.order_by
            where = line_item_obj.where
            limit = line_item_obj.limit
            table = line_item_obj.table
            viz_type = line_item_obj.viz_type

            # fetch connection details
            connection_obj = Connections.objects.get(pk=connection_id)
            if connection_obj.is_secure:
                is_secure = 'https'
            else:
                is_secure = 'http'
            ip = connection_obj.ip_address
            port = connection_obj.port
            query_endpoint = connection_obj.query_endpoint
            auth_endpoint = connection_obj.auth_endpoint
            username = connection_obj.username
            password = connection_obj.password
            cipher_key = connection_obj.cipher_key
            cipher = Fernet(cipher_key.encode('utf-8'))
            password = cipher.decrypt(password.encode('utf-8')).decode('utf-8')
            data_source_name = connection_obj.data_source_name
            # API urls
            auth_url = is_secure+"://"+ip+":"+port+auth_endpoint
            query_url = (is_secure
                         + "://"
                         + ip+":"
                         + port
                         + query_endpoint
                         + data_source_name)
            # API AUTHENTICATE call
            parameters = {"username": username, "password": password}
            headers = {'content-type': 'application/json'}

            # try:
            #     response = requests.post(auth_url, data=json.dumps(parameters), headers=headers)
            #     if (response.ok):
            #         response_dict = response.json()
            #         token_key = response_dict['id_token']
            # except Exception as e:
            #     print('error is', e)

            body = {
                "source": table,  # Table Name
                "fields": fields,  # Fields_list
                "groupBy": group_by,  # group_by_list
                "limit": int(limit),  # limit
                "conditionExpressions": where,  # Where
                "distinct": False,
                "orders": []   # order_by_list
            }
            # try:
            #     headers = {'content-type': 'application/json', 'Authorization': 'Bearer {0}'.format(token_key)}
            #     myResponse = requests.post(query_url, data=json.dumps(body), headers=headers)
            #     if (myResponse.ok):
            #         data = myResponse.json()

            # except Exception as e:
            #     print('error is', e)

            # test_list.append(dict([(viz_type, data), ('query_name', query_name)]))
        table_data = [

            {"state": "A", "price": 2},
            {"state": "B", "price": 5},
            {"state": "C", "price": 1},
            {"state": "D", "price": 3},
            {"state": "E", "price": 8},
            {"state": "F", "price": 12},
            {"state": "G", "price": 6},
            {"state": "H", "price": 4},
            {"state": "I", "price": 2},
        ]
        pie_chart = [

            {"state": "A", "price": 2},
            {"state": "B", "price": 5},
            {"state": "C", "price": 1},
            {"state": "D", "price": 3},
            {"state": "E", "price": 8},
            {"state": "F", "price": 12},
            {"state": "G", "price": 6},
            {"state": "H", "price": 4},
            {"state": "I", "price": 2},
        ]
        bar_chart = [
            {"state": "A", "price": 2, "quantity": 10, "measure-x": 5, "measure-y": 4, "measure-z": 7},
            {"state": "B", "price": 5, "quantity": 3, "measure-x": 8, "measure-y": 12, "measure-z": 9},
            {"state": "C", "price": 1, "quantity": 8, "measure-x": 10, "measure-y": 2, "measure-z": 5},
            {"state": "D", "price": 3, "quantity": 2, "measure-x": 12, "measure-y": 1, "measure-z": 2},
            {"state": "E", "price": 8, "quantity": 4, "measure-x": 2, "measure-y": 6, "measure-z": 3},
            {"state": "F", "price": 12, "quantity": 9, "measure-x": 1, "measure-y": 8, "measure-z": 9},
            {"state": "G", "price": 6, "quantity": 1, "measure-x": 6, "measure-y": 7, "measure-z": 14},
            {"state": "H", "price": 4, "quantity": 12, "measure-x": 9, "measure-y": 5, "measure-z": 6},
            {"state": "I", "price": 2, "quantity": 6, "measure-x": 7, "measure-y": 12, "measure-z": 11},
        ]

        test_list.append({'table': table_data, 'query_name': 'table_query'})
        test_list.append({'bar_chart': bar_chart, 'query_name': 'bar_chart_query'})
        test_list.append({'pie_chart': pie_chart, 'query_name': 'pie_chart_query'})

        return test_list, receiver_email, subject, mail_body, report_name


def draw_pie_chart(data, query_name):
    keys_list = sorted(data[0], key=lambda t: type(data[0][t]) == str, reverse=True)
    labels = [x[keys_list[0]] for x in data]
    sizes = [x[keys_list[1]] for x in data]
    fig1, ax1 = plt.subplots(figsize=(10, 10))
    ax1.pie(sizes, explode=None, labels=labels, autopct='%1.1f%%', shadow=False, startangle=90)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    ax1.set_title(query_name, y=1.03, fontsize=30)
    filename = str(uuid.uuid4())+'.png'
    filepath = report_charts_img_dir+filename
    plt.savefig(filepath)
    return filepath


def draw_bar_chart(data, query_name):
    keys_list = sorted(data[0], key=lambda t: type(data[0][t]) == str, reverse=True)
    data_dict = {}
    for key in keys_list:
        data_dict[key] = [x[key] for x in data]
    position_dict = {}
    barWidth = 0.15
    plt.figure(figsize=(15, 9))
    for i in range(1, len(keys_list)):
        if i == 1:
            position_dict[keys_list[i]] = np.arange(len(data_dict[keys_list[i]]))
        else:
            position_dict[keys_list[i]] = [x + barWidth for x in position_dict[keys_list[i-1]]]

    for k, v in position_dict.items():
        plt.bar(v, data_dict[k], width=barWidth, edgecolor='white', label=k)
    plt.xlabel(keys_list[0].title(), fontweight='bold')
    plt.xticks([r + barWidth for r in range(len(data_dict[keys_list[1]]))], data_dict[keys_list[0]])
    plt.legend(loc=9, bbox_to_anchor=(0.5, -0.07), ncol=5)
    plt.title(query_name, y=1.02, fontsize=30)
    filename = str(uuid.uuid4())+'.png'
    filepath = report_charts_img_dir+filename
    plt.savefig(filepath)
    return filepath


# func to generate pdf
def pdf_generator(data_list, report_name):
    try:
        context = {}
        for dict_obj in data_list:
            if 'bar_chart' in dict_obj:
                bar_chart_filepath = draw_bar_chart(dict_obj['bar_chart'], dict_obj['query_name'])
                context['bar_src'] = bar_chart_filepath
            if 'pie_chart' in dict_obj:
                pie_chart_filepath = draw_pie_chart(dict_obj['pie_chart'], dict_obj['query_name'])
                context['pie_src'] = pie_chart_filepath
            if 'table' in dict_obj:
                columns_list = list(dict_obj['table'][0].keys())
                context['columns_list'] = list(dict_obj['table'][0].keys())
                context['table_data'] = dict_obj['table']
                context['table_query_name'] = dict_obj['query_name']
        context['report_name'] = report_name
        j2_env = Environment(
            loader=FileSystemLoader(template_path),
            trim_blocks=True
        )
        j2_env.globals['STATIC_PREFIX'] = static_path,
        result = j2_env.get_template('reports_pdf.html').render(context)
        filename = "output.pdf"
        filepath = pdf_dir+filename
        ts = datetime.now().timestamp()
        st = datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        options = {
            'footer-line': '',
            'footer-right': 'Generated on %s' % (st),
            'footer-left': '[page] of [topage]'
        }
        pdfkit.from_string(result, filepath, options=options)
        return filepath, filename

    except Exception as e:
        print('error while pdf generation', e)


# func send email with attachement
@async
def send_email(filepath, filename, receiver_email, subject, mail_body):
    try:
        msg = MIMEMultipart()
        fromaddr = "cct.hiteshroy@gmail.com"
        toaddr = receiver_email
        msg['From'] = fromaddr
        msg['To'] = toaddr
        msg['Subject'] = subject
        body = mail_body
        msg.attach(MIMEText(body, 'plain'))
        attachment = open(filepath, "rb")
        part = MIMEBase('application', 'octet-stream')
        part.set_payload((attachment).read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
        msg.attach(part)
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(fromaddr, "Admin@123#")
        text = msg.as_string()
        server.sendmail(fromaddr, toaddr, text)
        server.quit()
    except Exception as e:
        print('error while sending email', e)


# functions call
def dag_call(**kwargs):
    try:
        data, receiver_email, subject, mail_body, report_name = getReportdata(kwargs['report_id'])
        filepath, filename = pdf_generator(data, report_name)
        send_email(filepath, filename, receiver_email, subject, mail_body)
        print('Record Sent Successfully')
    except Exception as e:
        print('error while calling functions', e)

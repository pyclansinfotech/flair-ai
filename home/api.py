from rest_framework import routers
from .views import ActivityViewSet


class ActivityRouterRegistry(object):

    @classmethod
    def buildrouters(self):
        router = routers.DefaultRouter()
        router.register(r'^api/v1/activity', ActivityViewSet)
        return router

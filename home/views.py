from rest_framework import viewsets
from rest_framework import permissions
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.contrib.auth import authenticate, login, logout
from utils.feeder import SourceFeed
from scrapbook.inkme import Ink
from home.models import Activity, ReportCreationData, AssignReportsData, \
    Connections, AirflowTasks, ReportLineItem
from .serializers import ActivitySerializer
from utils.actlog import Activitylogger
from subprocess import Popen, PIPE, check_output, STDOUT
from django.db import connection
from django.db.utils import ProgrammingError
from django.template.loader import render_to_string
import ast
import json
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
import sys
import base64
import os
from cryptography.fernet import Fernet
import subprocess
import airflow
import pytz
from dateutil.tz import tzlocal
import datetime
import re
import traceback


class ActivityViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Sources to be viewed or edited.
    """
    # Ink.info("Activity registered as an endpoint")
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)


def loginuser(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            data = {}
            data['totalsources'] = SourceFeed.totsources()
            data['sinmemory'] = SourceFeed.totsourcesinmemory()
            data['info'] = Activitylogger.read()
            return render(request, "home/home.html", {"data": data})
        else:
            return HttpResponseRedirect("/admin/login?next=/home")
    else:
        return render(request, "others/login.html")


@login_required()
def home(request):
    data = {}
    data['totalsources'] = SourceFeed.totsources()
    data['sinmemory'] = SourceFeed.totsourcesinmemory()
    data['info'] = Activitylogger.read()

    return render(request, "home/home.html", {"data": data})


def logoutuser(request):
    logout(request)
    return render(request, "others/login.html")


def pysql_execution(request):
    data = {}
    data['totalsources'] = SourceFeed.totsources()
    data['sinmemory'] = SourceFeed.totsourcesinmemory()
    data['info'] = Activitylogger.read()
    return render(request, 'home/pysql_execution.html', {"data": data})


def python_execution(request):
    if request.method == 'POST':
        query_text = request.POST.get('python_textarea')
        line = 'import django \ndjango.setup() \nfrom home.views import bar_chart, \
            line_chart, column_chart, pie_chart'
        f = open('query_text.py', 'w')
        f.seek(0, 0)
        f.write(line.rstrip('\r\n') + '\n' + query_text)
        f.close()
        op = execute__python_command()
        if True in op:
            op = op[0].decode("utf-8")[0:-1]
            pat = re.compile(r"\{'[a-z]*\_chart'.*$")
            m = re.search(pat, op)
            if m:
                found = m.group()
                final = ast.literal_eval(found)
                if isinstance(final, dict) and 'bar_chart' in final:
                    return JsonResponse({'bar_chart': final['bar_chart']})
                elif isinstance(final, dict) and'line_chart' in final:
                    return JsonResponse({'line_chart': final['line_chart']})
                elif isinstance(final, dict) and'column_chart' in final:
                    return JsonResponse(
                        {'column_chart': final['column_chart']}
                    )
                else:
                    return JsonResponse({'pie_chart': final['pie_chart']})
            else:
                return HttpResponse(op)
        else:
            op = op[0].decode("utf-8")[0:-1]
            pattern = re.compile(r'Traceback[\s|\S]*$')
            match = re.search(pattern, op)
            if match:
                return JsonResponse({'error': match.group()}, status=500)
            else:
                return JsonResponse({'error': op}, status=500)


def execute__python_command():
    process = subprocess.run(
        args=['python', 'query_text.py'],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    if process.returncode == 0:
        return process.stdout, True
    else:

        return process.stderr, False


def sql_execution(request):
    if request.method == 'POST':
        query_text = request.POST.get('sql_textarea')
        cursor = connection.cursor()
        try:
            cursor.execute(query_text)
            columns = [col[0] for col in cursor.description]
            result = [
                dict(zip(columns, row))
                for row in cursor.fetchall()
            ]
            html = render_to_string(
                'home/sql_execution_op_ajax.html',
                {"data": result, 'columns': columns},
                request=request
            )
            return JsonResponse({'html': html, 'chart_data': result})
        except ProgrammingError as e:
            return JsonResponse({'error': str(e)}, status=500)


def bar_chart(chart_data):
    print ({'bar_chart': chart_data})


def line_chart(chart_data):
    print ({'line_chart': chart_data})


def column_chart(chart_data):
    print ({'column_chart': chart_data})


def pie_chart(chart_data):
    print ({'pie_chart': chart_data})


# View Report Creation
def report_creation(request):
    # report_creation GET
    if request.method == 'GET':
        creation_data = ReportCreationData.objects.all()
        connection_objs = Connections.objects.all()
        connections_list = []
        connections_dict = {'id': '', 'connection_name': ''}
        for data in connection_objs:
            connections_dict['connection_name'] = data.connection_name
            connections_dict['id'] = data.id
            connections_list.append(connections_dict.copy())
        creation_data_list = []
        creation_data_dict = {
            'id': '', 'report_name': '', 'mail_body': '',
            'connection_name': '', 'subject': ''
        }
        for data in creation_data:
            creation_data_dict['id'] = data.id
            creation_data_dict['connection_name'] = data.connection_name
            creation_data_dict['report_name'] = data.report_name
            creation_data_dict['subject'] = data.subject
            creation_data_dict['mail_body'] = data.mail_body
            creation_data_list.append(creation_data_dict.copy())
        return render(
            request, 'home/reports.html',
            {
                'creation_data': creation_data_list,
                'connections': connections_list,
                'viz_type_choice': ReportLineItem.VIZ_TYPE_CHOICES
            }
        )

    # report_creation POST
    else:
        data_dict = request.POST.dict()
        print('data_dict', data_dict)
        connection_id = data_dict['select_connection']
        reportname = data_dict['reportname']
        qn = ReportCreationData.objects.filter(report_name=reportname)
        if(len(qn) > 0):
            return HttpResponse('report name already taken')
        else:
            report_creation_obj = ReportCreationData.objects.create(
                report_name=reportname,
                mail_body=data_dict['mail_body'],
                subject=data_dict['subject'],
                connection_id=connection_id,
                connection_name=Connections.objects.get(
                    pk=connection_id
                ).connection_name
            )
            report_creation_obj.save()

        return HttpResponse('success')


# DELETE CREATION REPORT
@csrf_exempt
def deleteCreationReport(request):
    report_id = request.POST['report_id']
    ReportCreationData.objects.filter(id=report_id).delete()
    return HttpResponse('success')


# View Report Creation
def save_report(request):
    if request.method == 'POST':
        data_dict = request.POST.dict()
        connection_id = data_dict['select_connection']
        reportname = data_dict['reportname']
        connection_data = Connections.objects.filter(pk=connection_id)
        connection_list = []
        connection_dict = {'connection_name': ''}
        for data in connection_data:
            connection_dict['connection_name'] = data.connection_name
        qn = ReportCreationData.objects.filter(report_name=reportname)
        if(len(qn) > 0):
            return HttpResponse('report name already taken')
        else:
            report_creation_obj = ReportCreationData.objects.create(
                connection_name=data.connection_name,
                report_name=reportname,
                mail_body=data_dict['mail_body'],
                subject=data_dict['subject'],
                connection_id=connection_id
            )
            report_creation_obj.save()
        return HttpResponse('success')
    else:
        return render(
            request, 'home/reports.html',
            {'connection_data': connection_list}
        )


# GETTING VALUE FROM ASSIGN REPORT DATA TABLE
@csrf_exempt
def assign_report_data(request):
    if request.method == 'GET':
        assign_data = AssignReportsData.objects.all()
        assign_reports_data_list = []
        assign_reports_data_dict = {
            'id': '', 'username': '', 'email': '',
            'report_name': '', 'channel': '', 'active': ''
        }
        for data in assign_data:
            report_name = ReportCreationData.objects.get(
                pk=data.report_creation_data_id).report_name
            assign_reports_data_dict['id'] = data.id
            assign_reports_data_dict['username'] = data.username
            assign_reports_data_dict['email'] = data.email
            assign_reports_data_dict['report_name'] = report_name
            assign_reports_data_dict['channel'] = data.channel
            assign_reports_data_dict['active'] = data.active
            assign_reports_data_list.append(assign_reports_data_dict.copy())
        report_objs = ReportCreationData.objects.all()
        report_data_list = []
        report_data_dict = {'report_name': '', 'report_id': ''}
        for data in report_objs:
            report_data_dict['report_name'] = data.report_name
            report_data_dict['report_id'] = data.id
            report_data_list.append(report_data_dict.copy())
        return render(
            request, 'home/assign_report.html',
            {
                'assign_reports_data': assign_reports_data_list,
                'reports': report_data_list
            }
        )
    else:
        data_dict = request.POST.dict()
        username = data_dict['username']
        email = data_dict['email']
        report_id = data_dict['report_id']
        channel = data_dict['channel']
        condition = data_dict['condition']
        active = True
        assign_report_obj = AssignReportsData.objects.create(
            username=username,
            email=email,
            report_creation_data_id=report_id,
            active=active,
            channel=channel,
            condition=condition
        )
        assign_report_obj.save()
        return HttpResponse('success')


@csrf_exempt
def add_report(request):
    if request.method == 'POST':
        data_dict = request.POST.dict()
        username = data_dict['username']
        email = data_dict['email']
        query_name = data_dict['query_name']
        channel = data_dict['channel']
        condition = data_dict['condition']
        active = True
        assign_report_obj = AssignReportsData.objects.create(
            username=username,
            email=email,
            query_name=query_name,
            active=active,
            channel=channel,
            condition=condition
        )
        assign_report_obj.save()
        return HttpResponse('success')
    else:
        html = render_to_string('home/assign_report.html')
        return HttpResponse(html)


@csrf_exempt
def addassign_report(request):
    if request.method == 'POST':
        data_dict = request.POST.dict()
        username = data_dict['username']
        email = data_dict['email']
        channel = data_dict['channel']
        condition = data_dict['condition']
        report_id = data_dict['report_id']
        active = True
        assign_report_obj = AssignReportsData.objects.create(
            username=username,
            email=email,
            active=active,
            channel=channel,
            condition=condition,
            report_creation_data_id=report_id
        )
        assign_report_obj.save()
        return HttpResponse('success')


# GET SELECTED CREATION REPORT DATA
@csrf_exempt
def getCreationReportData(request):
    report_id = request.POST['report_id']
    report_data = ReportCreationData.objects.filter(id=report_id)
    report_data_list = []
    report_dict = {'query_name': ''}
    for data in report_data:
        report_dict['id'] = data.id
        report_dict['query_name'] = data.query_name
        report_data_list.append(report_dict.copy())
    return JsonResponse({'report_data': report_data_list})


# DELETE ASSIGNED REPORT
@csrf_exempt
def deleteAssignedReport(request):
    report_id = request.POST['report_id']
    AssignReportsData.objects.filter(id=report_id).delete()
    return HttpResponse('success')


# GET SELECTED ASSIGNED REPORT DATA
@csrf_exempt
def getAssignedReportData(request):
    assigned_report_id = request.POST.get('assigned_report_id')
    print('assigned_report_id', assigned_report_id)
    report_data = AssignReportsData.objects.filter(id=assigned_report_id)
    report_data_list = []
    report_dict = {'id': '', 'username': '', 'email': '', 'report_name': ''}
    for data in report_data:
        report_dict['id'] = data.id
        report_dict['username'] = data.username
        report_dict['email'] = data.email
        report_dict['report_name'] = request.POST.get('report_name')
        report_data_list.append(report_dict.copy())
    return JsonResponse({'report_data': report_data_list})


# UPDATE  ASSIGNED REPORT DATA
@csrf_exempt
def updateAssignedReport(request):
    report_id = request.POST['report_id']
    email = request.POST['email']
    username = request.POST['username']
    assigned_report_id = request.POST['assigned_report_id']
    report = AssignReportsData.objects.get(id=assigned_report_id)
    report.username = username
    report.email = email
    report.report_creation_data_id = report_id
    report.save()
    return HttpResponse('success')


# GETTING VALUE FROM CONNECTION TABLE
@csrf_exempt
def getConnections(request):
    if request.method == 'GET':
        connections = Connections.objects.all()
        connections_list = []
        connections_dict = {
            'id': '', 'ip_address': '', 'port': '',
            'is_secure': '', 'query_endpoint': '', 'auth_endpoint': '',
            'username': '', 'password': '', 'data_source_name': '',
            'connection_name': ''
        }
        for data in connections:
            connections_dict['id'] = data.id
            connections_dict['ip_address'] = data.ip_address
            connections_dict['port'] = data.port
            connections_dict['is_secure'] = data.is_secure
            connections_dict['query_endpoint'] = data.query_endpoint
            connections_dict['auth_endpoint'] = data.auth_endpoint
            connections_dict['username'] = data.username
            connections_dict['password'] = data.password
            connections_dict['data_source_name'] = data.data_source_name
            connections_dict['connection_name'] = data.connection_name
            connections_list.append(connections_dict.copy())
        return render(
            request, 'home/connections.html',
            {'connections': connections_list}
        )
    else:
        return HttpResponse('/')


# SAVING CONNECTIONS_FORM DATA IN DATABASE
@csrf_exempt
def saveConnectionsData(request):
    if request.method == 'POST':
        data_dict = request.POST.dict()
        datasourcename = data_dict['datasourcename']
        connectionname = data_dict['connectionname']
        datasource_info = Connections.objects.filter(
            data_source_name=datasourcename
        )
        connection_info = Connections.objects.filter(
            connection_name=connectionname
        )
        if datasource_info:
            print('checkpoint 1')
            return HttpResponse('already existing datasource name')
        elif connection_info:
            print('checkpoint 2')
            return HttpResponse('already existing connection name')
        else:
            ip = data_dict['ip']
            port = data_dict['port']
            issecure = data_dict['issecure']
            if issecure == 'false':
                issecure = False
            else:
                issecure = True
            queryendpoint = data_dict['queryendpoint']
            authendpoint = data_dict['authendpoint']
            username = data_dict['username']
            password = data_dict['password']
            password = password.encode('utf-8')
            cipher_key = Fernet.generate_key()
            cipher = Fernet(cipher_key)
            encrypted_password = cipher.encrypt(password)
            conn_obj = Connections.objects.create(
                data_source_name=datasourcename,
                connection_name=connectionname,
                ip_address=ip,
                port=port,
                is_secure=issecure,
                query_endpoint=queryendpoint,
                auth_endpoint=authendpoint,
                username=username,
                password=encrypted_password,
                cipher_key=cipher_key
            )
            conn_obj.save()
            print('checkpoint 1')
            return HttpResponse('success')


# DELETE CONNECTION REPORT
@csrf_exempt
def deleteConnectionData(request):
    connection_id = request.POST['connection_id']
    Connections.objects.filter(id=connection_id).delete()
    return HttpResponse('success')


# GET SELECTED CONNECTION ROW DATA
@csrf_exempt
def getConnectionData(request):
    connection_id = request.POST['connection_id']
    connection_data = Connections.objects.filter(id=connection_id)
    connection_data_list = []
    connection_dict = {
        'id': '', 'ip_address': '', 'port': '', 'is_secure': '',
        'query_endpoint': '', 'auth_endpoint': '', 'username': '',
        'password': '', 'data_source_name': '', 'connection_name': ''
    }
    for data in connection_data:
        connection_dict['id'] = data.id
        connection_dict['ip_address'] = data.ip_address
        connection_dict['port'] = data.port
        connection_dict['is_secure'] = data.is_secure
        connection_dict['query_endpoint'] = data.query_endpoint
        connection_dict['auth_endpoint'] = data.auth_endpoint
        connection_dict['username'] = data.username
        connection_dict['password'] = data.password
        connection_dict['data_source_name'] = data.data_source_name
        connection_dict['connection_name'] = data.connection_name
        connection_data_list.append(connection_dict.copy())
    return JsonResponse({'connection_data': connection_data_list})


# UPDATE  ASSIGNED REPORT DATA
@csrf_exempt
def updateConnectionData(request):
    connection_id = request.POST['connection_id']
    ip_address = request.POST['ip']
    port = request.POST['port']
    is_secure = request.POST['issecure']
    query_endpoint = request.POST['query_endpoint']
    auth_endpoint = request.POST['auth_endpoint']
    username = request.POST['username']
    password = request.POST['password']
    datasource_name = request.POST['datasource_name']
    connection_name = request.POST['connection_name']
    connection = Connections.objects.get(id=connection_id)
    connection.ip_address = ip_address
    connection.port = port
    connection.is_secure = is_secure
    connection.query_endpoint = query_endpoint
    connection.auth_endpoint = auth_endpoint
    connection.username = username
    connection.password = password
    connection.data_source_name = datasource_name
    connection.connection_name = connection_name
    connection.save()
    return HttpResponse('success')


# SAVING DAG SCHEDULER DAT
@csrf_exempt
def dagScheduler(request):
    if request.method == 'POST':
        data_dict = request.POST.dict()
        start_date = data_dict['date']
        time = data_dict['time']
        timezone = data_dict['timezone']
        str_new = time.split(' ')
        hours = str_new[1]
        minutes = str_new[0]
        ms = '00'
        ist_time = hours+':'+minutes+':'+ms
        dynamic_datetime = start_date+' '+ist_time
        local_tz = pytz.timezone(timezone)
        datetime_without_tz = datetime.datetime.strptime(
            dynamic_datetime, "%Y-%m-%d %H:%M:%S"
        )
        datetime_with_tz = local_tz.localize(  # No daylight saving time
            datetime_without_tz, is_dst=None
        )
        datetime_in_utc = datetime_with_tz.astimezone(pytz.utc)
        str3 = datetime_in_utc.strftime('%Y-%m-%d %H:%M:%S %Z')
        date_str = str3.split(' ')
        time_str = date_str[1]
        split_time = time_str.split(':')
        utc_minutes = split_time[1]
        utc_hours = split_time[0]
        str_new[0] = utc_minutes
        str_new[1] = utc_hours
        utc_time_cron = ' '.join(str_new)
        report_id = data_dict['report_id']
        if AirflowTasks.objects.filter(assign_task_id=report_id).exists():
            AirflowTasks.objects.filter(assign_task_id=report_id).update(
                schedule_interval=utc_time_cron, start_date=start_date
            )
        else:
            task_obj = AirflowTasks.objects.create(
                assign_task_id=report_id, schedule_interval=utc_time_cron,
                start_date=start_date
            )
            task_obj.save()
        return HttpResponse('success')
    else:
        return render(request, 'home/assign_report.html')


# Create REPORT LINE ITEM
def reportLineItem(request):
    if request.method == 'POST':
        data_dict = request.POST.dict()
        report_line_item_obj = ReportLineItem.objects.create(
            report_id=data_dict['select_report'],
            query_name=data_dict['report_query_name'],
            fields=data_dict['report_fields'],
            group_by=data_dict['group_by'],
            order_by=data_dict['order_by'],
            where=data_dict['where'],
            limit=data_dict['limit'],
            table=data_dict['table_name'],
            viz_type=data_dict['select_viztype']
        )
        report_line_item_obj.save()
        return HttpResponse('success')
    else:
        reports = ReportCreationData.objects.all()
        report_line_items = ReportLineItem.objects.all()
        return render(
            request,
            'home/report_line_items.html',
            {
                'reports': reports, 'report_items': report_line_items,
                'viz_type_choice': ReportLineItem.VIZ_TYPE_CHOICES
            }
        )


@csrf_exempt
def getReportItemData(request):
    if request.method == 'POST':
        report_item_id = request.POST.get('report_item_id')
        report_item_obj = ReportLineItem.objects.get(pk=report_item_id)
        report_data = {}
        report_data['query_name'] = report_item_obj.query_name
        report_data['fields'] = report_item_obj.fields
        report_data['group_by'] = report_item_obj.group_by
        report_data['order_by'] = report_item_obj.order_by
        report_data['where'] = report_item_obj.where
        report_data['limit'] = report_item_obj.limit
        report_data['table'] = report_item_obj.table
        report_data['viz_type'] = report_item_obj.viz_type
        return JsonResponse({'report_item_data': report_data})


def updateReportItem(request):
    if request.method == 'POST':
        data_dict = request.POST.dict()
        report_item_obj = ReportLineItem.objects.get(
            pk=data_dict['report_item_id']
        )
        report_item_obj.report_id = data_dict['edit_select_report']
        report_item_obj.query_name = data_dict['edit_report_queryname']
        report_item_obj.fields = data_dict['edit_report_fields']
        report_item_obj.group_by = data_dict['edit_group_by']
        report_item_obj.order_by = data_dict['edit_order_by']
        report_item_obj.where = data_dict['edit_where']
        report_item_obj.limit = data_dict['edit_limit']
        report_item_obj.table = data_dict['edit_table_name']
        report_item_obj.viz_type = data_dict['edit_select_viztype']
        report_item_obj.save()
        return HttpResponse('success')


@csrf_exempt
def deleteReportItem(request):
    ReportLineItem.objects.filter(
        pk=request.POST.get('report_item_id')
    ).delete()
    return HttpResponse('success')


def createReportItem(request):
    if request.method == 'POST':
        data_dict = request.POST.dict()
        report_item_obj = ReportLineItem.objects.create(
            report_id=data_dict['create_select_report'],
            query_name=data_dict['create_report_queryname'],
            fields=data_dict['create_report_fields'],
            group_by=data_dict['create_group_by'],
            order_by=data_dict['create_order_by'],
            where=data_dict['create_where'],
            limit=data_dict['create_limit'],
            table=data_dict['create_table_name'],
            viz_type=data_dict['create_select_viztype']
        )
        report_item_obj.save()
        return HttpResponse('success')

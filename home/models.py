from django.db import models
import uuid
from django.contrib.auth.models import User
from django.utils import timezone
from django.contrib.postgres.fields import JSONField
import pytz


# Create your models here.
class Activity(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    type = models.CharField(max_length=250)
    activity_timestamp = models.DateTimeField(default=timezone.now)
    message = models.TextField(null=True)
    display = models.BooleanField(default=True)
    styles = models.CharField(max_length=100, null=True)
    icon = models.CharField(max_length=100, null=True)
    user = models.ForeignKey(User)

    class Meta:
        permissions = (
            ("read_activity", "Read Access to Activities"),
        )


# Connections
class Connections(models.Model):
    ip_address = models.GenericIPAddressField(max_length=100)
    port = models.CharField(max_length=100)
    is_secure = models.BooleanField(default=False)
    query_endpoint = models.CharField(max_length=150)
    auth_endpoint = models.CharField(max_length=150)
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    connection_name = models.CharField(max_length=100)
    data_source_name = models.CharField(max_length=100)
    cipher_key = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return self.connection_name


# Report Creation
class ReportCreationData(models.Model):
    connection = models.ForeignKey(
        'Connections',
        on_delete=models.CASCADE
    )
    connection_name = models.CharField(max_length=100, null=True)
    report_name = models.CharField(max_length=100)
    subject = models.CharField(max_length=250)
    mail_body = models.TextField()

    def __str__(self):
        return str(self.report_name)


# Reports line items
class ReportLineItem(models.Model):
    VIZ_TYPE_CHOICES = (
        ('table', 'Table'),
        ('pie_chart', 'Pie Chart'),
        ('bar_chart', 'Bar Chart'),
    )
    report = models.ForeignKey(
        'ReportCreationData',
        on_delete=models.CASCADE
    )
    query_name = models.CharField(max_length=100)
    fields = JSONField()
    group_by = JSONField(null=True)
    order_by = JSONField(null=True)
    where = JSONField(null=True)
    limit = models.CharField(max_length=20, null=True)
    table = models.CharField(max_length=250)
    viz_type = models.CharField(
        max_length=20,
        choices=VIZ_TYPE_CHOICES
    )

    def __str__(self):
        return str(self.id)


# Assign Reports
class AssignReportsData(models.Model):
    report_creation_data = models.ForeignKey(
        'ReportCreationData',
        on_delete=models.CASCADE
    )
    username = models.CharField(max_length=100)
    email = models.CharField(max_length=150)
    active = models.BooleanField(default=False)
    channel = models.CharField(max_length=250)
    condition = models.CharField(max_length=100, null=True)

    def __str__(self):
        return str(self.id)


# Airflow Task(DAG SCHEDULER)
class AirflowTasks(models.Model):
    assign_task = models.ForeignKey(
        'AssignReportsData',
        on_delete=models.CASCADE
    )
    schedule_interval = models.CharField(max_length=250)
    start_date = models.DateField()
    timezone_choices = [(obj, obj) for obj in pytz.all_timezones]
    timezone = models.CharField(
        max_length=80,
        choices=timezone_choices
    )

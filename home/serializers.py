from .models import Activity
from rest_framework import serializers


class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = (
            'id',
            'type',
            'activity_timestamp',
            'message',
            'display',
            'styles',
            'icon',
            'user',
        )

from django.contrib.auth.models import User
from rest_framework import generics, permissions, renderers, viewsets
from rest_framework.decorators import api_view, detail_route
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from home.models import Activity, ReportCreationData, AssignReportsData, \
    Connections, AirflowTasks, ReportLineItem
from reports_apis.serializers import ReportSerializer, ConnectionsSerializer, \
    AssignReportsSerializer, AirflowTasksSerializer, SourceSerializer, \
    FeatureSerializer, FieldTypeSerializer, DataTypeSerializer, \
    ActivitySerializer, ReportLineItemSerializer
from browser.models import Source, Feature, FieldType, DataType


# CONNECTION
class ConnectionViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = Connections.objects.all()
    serializer_class = ConnectionsSerializer
    permission_classes = (IsAuthenticated,)

    @detail_route(renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        connection = self.get_object()
        return Response(connection.highlighted)


# REPORTS
class ReportsViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = ReportCreationData.objects.all()
    serializer_class = ReportSerializer
    permission_classes = (IsAuthenticated,)

    @detail_route(renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        report = self.get_object()
        return Response(report.highlighted)


# REPORT LINE ITEM
class ReportLineItemViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = ReportLineItem.objects.all()
    serializer_class = ReportLineItemSerializer
    permission_classes = (IsAuthenticated,)

    @detail_route(renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        report = self.get_object()
        return Response(report.highlighted)


# ASSIGN REPORT
class AssignReportsViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = AssignReportsData.objects.all()
    serializer_class = AssignReportsSerializer
    permission_classes = (IsAuthenticated,)

    @detail_route(renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        assign_report = self.get_object()
        return Response(assign_report.highlighted)


# AIRFLOW TASK (DAG SCHEDULER)
class AirflowTasksViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = AirflowTasks.objects.all()
    serializer_class = AirflowTasksSerializer
    permission_classes = (IsAuthenticated,)

    @detail_route(renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        airflow_task = self.get_object()
        return Response(airflow_task.highlighted)


# Source
class SourceViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """

    queryset = Source.objects.all()
    serializer_class = SourceSerializer
    permission_classes = (IsAuthenticated, IsAdminUser)

    @detail_route(renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        source = self.get_object()
        return Response(source.highlighted)


# Feature
class FeatureViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """

    queryset = Feature.objects.all()
    serializer_class = FeatureSerializer
    permission_classes = (IsAuthenticated, IsAdminUser)

    @detail_route(renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        feature = self.get_object()
        return Response(feature.highlighted)


# FieldType
class FieldTypeViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """

    queryset = FieldType.objects.all()
    serializer_class = FieldTypeSerializer
    permission_classes = (IsAuthenticated, IsAdminUser)

    @detail_route(renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        field_type = self.get_object()
        return Response(field_type.highlighted)


# DataType
class DataTypeViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """

    queryset = DataType.objects.all()
    serializer_class = DataTypeSerializer
    permission_classes = (IsAuthenticated, IsAdminUser)

    @detail_route(renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        data_type = self.get_object()
        return Response(data_type.highlighted)


# Activity
class ActivityViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """

    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer
    permission_classes = (IsAuthenticated, IsAdminUser)

    @detail_route(renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        activity = self.get_object()
        return Response(activity.highlighted)

from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token
from reports_apis import views

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'connections', views.ConnectionViewSet)
router.register(r'reports', views.ReportsViewSet)
router.register(r'report-line-item', views.ReportLineItemViewSet)
router.register(r'assign-reports', views.AssignReportsViewSet)
router.register(r'airflow-tasks', views.AirflowTasksViewSet)
router.register(r'source', views.SourceViewSet)
router.register(r'feature', views.FeatureViewSet)
router.register(r'field-type', views.FieldTypeViewSet)
router.register(r'data-type', views.DataTypeViewSet)
router.register(r'activity', views.ActivityViewSet)


# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-token-auth/', obtain_jwt_token),
]

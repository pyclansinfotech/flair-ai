from django.contrib.auth.models import User
from rest_framework import serializers
from home.models import Activity, ReportCreationData, AssignReportsData, \
    Connections, AirflowTasks, ReportLineItem
import pytz
import datetime
from cryptography.fernet import Fernet
import json
from browser.models import Source, Feature, FieldType, DataType
from django.contrib.auth.models import User
import os
from rest_framework.validators import UniqueValidator


# CONNECTIONS
class ConnectionsSerializer(serializers.HyperlinkedModelSerializer):
    password = serializers.CharField(style={'input_type': 'password'})

    class Meta:
        model = Connections
        fields = (
            'url',
            'id',
            'ip_address',
            'port',
            'is_secure',
            'query_endpoint',
            'auth_endpoint',
            'username',
            'password',
            'connection_name',
            'data_source_name'
        )
        extra_kwargs = {
            'connection_name': {
                'validators': [UniqueValidator(
                    queryset=Connections.objects.all(),
                    message="connection name already exists."
                    )
                ],
            },
            'data_source_name': {
                'validators': [UniqueValidator(
                    queryset=Connections.objects.all(),
                    message="data source name already exists."
                    )
                ],
            }
        }

    def create(self, validated_data):
        data = validated_data.copy()
        password = data['password']
        password = password.encode('utf-8')
        cipher_key = Fernet.generate_key()
        cipher = Fernet(cipher_key)
        encrypted_password = cipher.encrypt(password)
        data['cipher_key'] = cipher_key
        data['password'] = encrypted_password

        return super(ConnectionsSerializer, self).create(data)


# REPORT CREATION
class ReportSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReportCreationData
        fields = (
            'url',
            'id',
            'connection',
            'report_name',
            'subject',
            'mail_body',
        )
        extra_kwargs = {
            'report_name': {
                'validators': [UniqueValidator(
                    queryset=ReportCreationData.objects.all(),
                    message="report name already exists."
                    )
                ],
            }
        }

    def create(self, validated_data):
        data = validated_data.copy()
        data['connection_name'] = data['connection'].connection_name

        return super(ReportSerializer, self).create(data)


# REPORT LINE ITEM
class ReportLineItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReportLineItem
        fields = (
            'url',
            'id',
            'report',
            'query_name',
            'fields',
            'group_by',
            'order_by',
            'where',
            'limit',
            'table',
            'viz_type'
        )


# ASSIGN REPORT
class AssignReportsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AssignReportsData
        fields = (
            'url',
            'id',
            'report_creation_data',
            'username',
            'email',
            'active',
            'channel',
            'condition'
        )


# AIRFLOW TASK(DAG SCHEDULER)
class AirflowTasksSerializer(serializers.ModelSerializer):
    class Meta:
        model = AirflowTasks
        fields = (
            'url',
            'id',
            'assign_task',
            'schedule_interval',
            'start_date',
            'timezone'
        )

    def make_utc_time(self, start_date, time, timezone):
        str_new = time.split(' ')
        hours = str_new[1]
        minutes = str_new[0]
        ms = '00'
        ist_time = hours+':'+minutes+':'+ms
        str_start_date = datetime.datetime.strftime(start_date, "%Y-%m-%d")
        dynamic_datetime = str_start_date+' '+ist_time
        local_tz = pytz.timezone(timezone)
        datetime_without_tz = datetime.datetime.strptime(
            dynamic_datetime,
            "%Y-%m-%d %H:%M:%S"
        )
        datetime_with_tz = local_tz.localize(  # No daylight saving time
            datetime_without_tz,
            is_dst=None
        )
        datetime_in_utc = datetime_with_tz.astimezone(pytz.utc)
        str3 = datetime_in_utc.strftime('%Y-%m-%d %H:%M:%S %Z')
        date_str = str3.split(' ')
        time_str = date_str[1]
        split_time = time_str.split(':')
        utc_minutes = split_time[1]
        utc_hours = split_time[0]
        str_new[0] = utc_minutes
        str_new[1] = utc_hours
        utc_time_cron = ' '.join(str_new)
        return utc_time_cron

    def create(self, validated_data):
        data = validated_data.copy()
        start_date = data['start_date']
        time = data['schedule_interval']
        timezone = data['timezone']
        utc_time_cron = self.make_utc_time(start_date, time, timezone)
        data['schedule_interval'] = utc_time_cron
        return super(AirflowTasksSerializer, self).create(data)

    def update(self, instance, validated_data):
        data = validated_data.copy()
        start_date = data['start_date']
        time = data['schedule_interval']
        timezone = data['timezone']
        utc_time_cron = self.make_utc_time(start_date, time, timezone)
        instance.schedule_interval = utc_time_cron
        instance.start_date = start_date
        instance.timezone = timezone
        instance.save()
        return instance


# User
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'username',
            'is_active',
            'is_superuser',
        )


# Source
class SourceSerializer(serializers.HyperlinkedModelSerializer):
    extension = serializers.CharField(required=False)

    class Meta:
        model = Source
        fields = (
            'url',
            'id',
            'table',
            'docfile',
            'file_location',
            'ods_file_location',
            'identifier',
            'in_memory',
            'analysed',
            'extension',
        )

    def create(self, validated_data):
        data = validated_data.copy()
        data['user'] = self.context['request'].user
        data['file_location'], data['extension'] = os.path.splitext(
            str(data['docfile'])
        )

        return super(SourceSerializer, self).create(data)

    def update(self, instance, validated_data):
        data = validated_data.copy()
        if data['docfile'] is None:
            data['docfile'] = instance.docfile
            return super(SourceSerializer, self).update(instance, data)
        else:
            data['file_location'], data['extension'] = os.path.splitext(
                str(data['docfile'])
            )
            return super(SourceSerializer, self).update(instance, data)


# Feature
class FeatureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feature
        fields = (
            'url',
            'id',
            'field_name',
            'density',
            'source_link',
            'fieldtype_link',
            'datatype_link',
        )


# FieldType
class FieldTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = FieldType
        fields = (
            'url',
            'id',
            'field_type'
        )


# DataType
class DataTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = DataType
        fields = (
            'url',
            'id',
            'data_type',
        )


# Activity
class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = (
            'url',
            'id',
            'type',
            'activity_timestamp',
            'message',
            'display',
            'styles',
            'icon',
            'user',
        )

import pandas as pd
from browser.models import Source, Feature, FieldType, DataType
import os
import json
from ai.settings import BASE_DIR

class DataProfiler(object):

    def __init__(self, id):
        self.id = id
        self.source = Source.objects.get(id=self.id)
        self.relative_path = BASE_DIR + '/' + str(self.source.docfile)
        self.field_types = self.get_fieldtypes()
        self.data_types = self.get_datatypes()
        self.datatype_mapping = {
            "int": "Integer",
            "float": "Float",
            "long": "Integer",
            "complex": "String",
            "str": "String",
            "bytes": "String",
            "tuple": "String",
            "frozen set": "String",
            "bool": "Boolean",
            "array": "String"
        }
        self.fieldtype_mapping = {
            "int": "Measure",
            "float": "Measure",
            "long": "Measure",
            "complex": "Dimension",
            "str": "Dimension",
            "bytes": "Dimension",
            "tuple": "Dimension",
            "frozen set": "Dimension",
            "bool": "Dimension",
            "array": "Dimension"
        }

    def analyse(self):
        if self.source.extension == '.json':
            content = {}
            content['sourceid'] = self.id
            content['fields'] = self.construct_profile(self.read_json())
            content['data_types'] = list(self.data_types.keys())
            content['field_types'] = list(self.field_types.keys())
            return content
        elif self.source.extension == '.csv':
            content = {}
            content['sourceid'] = self.id
            content['fields'] = self.construct_profile(self.read_csv())
            content['data_types'] = list(self.data_types.keys())
            content['field_types'] = list(self.field_types.keys())
            return content

    def read_json(self):
        return json.loads(open(self.relative_path).readline())

    def read_csv(self):
        data = {}
        sample = open(self.relative_path).readlines()[0:2]
        columns = sample[0].rstrip("\n").split(',')
        values = sample[1].rstrip("\n").split(',')
        if len(columns) == len(values):
            for idx, column in enumerate(columns):
                data[column] = values[idx]
            return self.cleansed_data(data)
        else:
            return {}

    def cleansed_data(self, data):
        for key in data:
            if self.inttest(data[key]) is True:
                data[key] = int(data[key])
            elif self.floattest(data[key]) is True:
                data[key] = float(data[key])
        print(str(data))
        return data

    def inttest(self, value):
        try:
            int(value)
            return True
        except Exception as e:
            return False

    def floattest(self, value):
        try:
            float(value)
            return True
        except Exception as e:
            return False

    def construct_profile(self, line):
        data_profile = {}
        for key in line.keys():
            if not Feature.objects.filter(source_link=self.source, field_name=key).exists():
                try:
                    if type(line[key]) is str:
                        data_profile[key] = {}
                        data_profile[key]['datatype'] = self.get_datatypes()
                        data_profile[key]['datatype'][self.datatype_mapping['str']] = "selected"
                        data_profile[key]['fieldtype'] = self.get_fieldtypes()
                        data_profile[key]['fieldtype'][self.fieldtype_mapping['str']] = "checked"
                    elif type(line[key]) is int:
                        data_profile[key] = {}
                        data_profile[key]['datatype'] = self.get_datatypes()
                        data_profile[key]['datatype'][self.datatype_mapping['int']] = "selected"
                        data_profile[key]['fieldtype'] = self.get_fieldtypes()
                        data_profile[key]['fieldtype'][self.fieldtype_mapping['int']] = "checked"
                    elif type(line[key]) is float:
                        data_profile[key] = {}
                        data_profile[key]['datatype'] = self.get_datatypes()
                        data_profile[key]['datatype'][self.datatype_mapping['float']] = "selected"
                        data_profile[key]['fieldtype'] = self.get_fieldtypes()
                        data_profile[key]['fieldtype'][self.fieldtype_mapping['float']] = "checked"
                    elif type(line[key]) is complex:
                        data_profile[key] = {}
                        data_profile[key]['datatype'] = self.get_datatypes()
                        data_profile[key]['datatype'][self.datatype_mapping['complex']] = "selected"
                        data_profile[key]['fieldtype'] = self.get_fieldtypes()
                        data_profile[key]['fieldtype'][self.fieldtype_mapping['complex']] = "checked"
                    elif type(line[key]) is bool:
                        data_profile[key] = {}
                        data_profile[key]['datatype'] = self.get_datatypes()
                        data_profile[key]['datatype'][self.datatype_mapping['bool']] = "selected"
                        data_profile[key]['fieldtype'] = self.get_fieldtypes()
                        data_profile[key]['fieldtype'][self.fieldtype_mapping['bool']] = "checked"
                    data_profile[key]['id'] = ""
                except Exception as e:
                    print(type(line[key]), "Not found in mapping dictionary")
                    print(str(e))
            else:
                try:
                    ex_feature = Feature.objects.filter(source_link=self.source, field_name=key)
                    for field in ex_feature:
                        data_profile[field.field_name] = {}
                        data_profile[field.field_name]['datatype'] = self.get_datatypes()
                        data_profile[field.field_name]['datatype'][field.datatype_link.data_type] = "selected"
                        data_profile[field.field_name]['fieldtype'] = self.get_fieldtypes()
                        data_profile[field.field_name]['fieldtype'][field.fieldtype_link.field_type] = "checked"
                        data_profile[field.field_name]['id'] = field.id
                except Exception as es:
                    print(es, 'From existing feature')
        return data_profile


    def get_fieldtypes(self):
        fieldtype_lookup = {}
        fieldtypes = FieldType.objects.all().order_by('id')
        for fieldtype in fieldtypes:
            fieldtype_lookup[fieldtype.field_type] = ""
        return fieldtype_lookup

    def get_datatypes(self):
        datatype_lookup = {}
        datatypes = DataType.objects.all().order_by('id')
        for datatype in datatypes:
            datatype_lookup[datatype.data_type] = ""
        return datatype_lookup

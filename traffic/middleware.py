from scrapbook.inkme import Ink


class Logging(object):
    """
    Provides full logging of requests and responses
    """
    _initial_http_body = None

    def __init__(self, get_response):
        Ink.info("Initializing request logging...")
        self.get_response = get_response

    def __call__(self, request):
        Ink.info("{0} >> {1}".format(request.method, request.path))
        return self.get_response(request)

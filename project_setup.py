import os
import subprocess
import sys
import fileinput
import webbrowser
import pip
# PROJECT DIR PARENT DIR FOR VIRTUAL ENVIRONMENT
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# DIR VIRTUAL ENVIRONMENT
venv_dir = os.path.join(BASE_DIR, 'fvenv')
# CREATE VIRTUAL ENVIROMENT
subprocess.run(args=['virtualenv', '-p', 'python3', venv_dir])
# ACTIVATE VIRTUAL ENVIRONMENT
os.popen('/bin/bash --rcfile %s' % (venv_dir + '/bin/activate'))
# INSTALL DEPENDENCIES FOR PROJECT
pip3 = os.path.join(venv_dir, 'bin', 'pip3')
subprocess.run(
    args=[pip3, 'install', '-r', os.path.join(
        BASE_DIR, 'flair-ai/requirements.txt')]
    )
# IMPORT PSYCOPG2 MODULE FOR CREATING POSTGRES DATABASE
from psycopg2 import connect
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
# CREATE POSTGRES DATABASE OF FLAIRAI PROJECT
con = None
dbflair = "flairap"
dbairflow = "airflow"
con = connect(
    dbname='postgres', user='postgres', host='localhost', password='postgres'
    )
con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)  # <-- ADD THIS LINE
cur = con.cursor()
cur.execute("CREATE DATABASE %s  ;" % dbflair)
# CREATE POSTGRES DATABASE OF AIRFLOW
cur.execute("CREATE DATABASE %s  ;" % dbairflow)
cur.close()
con.close()
# FLAIRAI  MAKEMIGRATIONS
subprocess.run(args=[venv_dir+'/bin/python', 'manage.py', 'makemigrations'])
# FLAIRAI MIGRATE
subprocess.run(args=[venv_dir+'/bin/python', 'manage.py', 'migrate'])
# CREATE AIRFLOW HOME DIR
airflow_home_path = os.path.abspath(os.path.join(os.getcwd(), 'airflow_home'))
try:
    os.mkdir(airflow_home_path)
except FileExistsError:
    subprocess.run(args=['rm', '-r', 'airflow_home'])
os.environ['AIRFLOW_HOME'] = airflow_home_path
# CREATE DEFAULT AIRFLOW CONFIG FILE
subprocess.run(args=['airflow', 'version'])
# LINES TO REPLACE IN AIRFLOW CONFIG FILE
sqlite_conn_str = "sql_alchemy_conn = sqlite:///"+airflow_home_path+'/airflow.db'
postgres_conn_str = "sql_alchemy_conn = postgresql+psycopg2://postgres:postgres@localhost/airflow"
old_flag_val = "dags_are_paused_at_creation = True"
new_flag_val = "dags_are_paused_at_creation = False"
findlines = []
findlines.append(sqlite_conn_str)
findlines.append(old_flag_val)
replacelines = []
replacelines.append(postgres_conn_str)
replacelines.append(new_flag_val)
find_replace = dict(zip(findlines, replacelines))
with open(airflow_home_path+'/airflow.cfg') as data:
    with open(airflow_home_path+'/new_airflow.cfg', 'w') as new_data:
        for line in data:
            for key in find_replace:
                if key in line:
                    line = line.replace(key, find_replace[key])
            new_data.write(line)
subprocess.run(args=['rm',  airflow_home_path+'/airflow.cfg'])
subprocess.run(
    args=[
        'mv', airflow_home_path+'/new_airflow.cfg',
        airflow_home_path+'/airflow.cfg'
        ]
    )
# INITIALISE AIRFLOW DB
subprocess.run(args=['airflow', 'initdb'])
# AIRFLOW DAG DIR
dag_dir_path = airflow_home_path+'/dags/'
os.mkdir(dag_dir_path)
# COPY SCHEDULER.PY FILE TO DAGS DIR
subprocess.run(args=['cp', 'scheduler_bk.py', dag_dir_path])
subprocess.run(
    args=['mv', dag_dir_path+'scheduler_bk.py', dag_dir_path+'scheduler.py']
    )
# RUN AIRFLOW SCHEDULER
subprocess.Popen(args=['nohup', 'airflow', 'scheduler'])
# RUN AIRFLOW WEBSERVER
subprocess.Popen(args=['nohup', 'airflow', 'webserver'])
# LOAD BROWSER JSON DATA
subprocess.run(args=[venv_dir+'/bin/python', 'manage.py', 'loaddata', 'browser.json'])
# RUN FLAIRAI PROJECT
subprocess.run(args=[venv_dir+'/bin/python', 'manage.py', 'runserver'])
# RUN IN WEB BROWSER
api_url = "http://127.0.0.1:8000/api/"
webbrowser.open(api_url, new=2, autoraise=True)


$(document).ready(function () {

    // file upload
    var frm = $('#source_upload');
    frm.submit(function(event) {
        var formData = new FormData();
        formData.append('docfile', $('#id_docfile')[0].files[0]); 
        formData.append('csrfmiddlewaretoken', $('#csrf').val());
        formData.append('table', $('#id_table').val());
        $.ajax({            
            url: '/manager/fileupload/',
            data: formData,            
            type: 'POST',
            contentType: false,
            processData: false,
            statusCode: {
                200: function (res) {
                    $.ajax({
                      url:"/manager/profiling/"+res.id+"/",
                      type:'GET',                      
                      success: function(res){
                        $('#form_wizard').html(res);
                      }
                    });
                },
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus)
            }
        });
        event.preventDefault();
    });

    // step_profiling_form
    var step_profiling_form = $('#step_profiling_form');
    step_profiling_form.submit(function(event) {
      var id = $('#id_source').val();
      $.ajax({
        url:'/manager/profiling/'+id+'/',
        type:'POST',
        data:$(this).serialize(),
        statusCode: {
            200: function (res) {
                $.ajax({
                  url:"/manager/finish/"+res.id+"/",
                  type:'GET',                      
                  success: function(res){                
                    if($('#source_detail_form_wizard').is(":visible")){
                      $('#source_detail_form_wizard').html(res);
                    }
                    else if($('#source_conversion_form_wizard').is(":visible")){
                      $('#source_conversion_form_wizard').html(res);
                    }
                    else{                
                      $('#form_wizard').html(res);
                    }  
                  }
                });
            },
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus)
        }
      })

      event.preventDefault();
    });

    //modal source detail
    $('.source_detail_modal').on('click', function(e){
      e.preventDefault();
      var source_id = $(this).data('id');
      $.ajax({
        url:"/manager/profiling/"+source_id+"/",
        type:'GET',                      
        success: function(res){
          $('#source_detail_form_wizard').html(res);
          $('#sourcedetailsModal').modal('show');
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus)
        }
      });
    });

    //modal source detail
    $('.source_conversion_modal').on('click', function(e){
      e.preventDefault();
      var source_conversion_id = $(this).data('id');
      $.ajax({
        url:"/manager/profiling/"+source_conversion_id+"/",
        type:'GET',                      
        success: function(res){
          $('#source_conversion_form_wizard').html(res);
          $('#sourceConversionModal').modal('show');
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus)
        }
      });
    });

});


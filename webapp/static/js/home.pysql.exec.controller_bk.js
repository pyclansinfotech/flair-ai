$(document).ready(function() {
    var new_flag = ''
    $('#play_btn').on('click', function() {
        if ($('#python').hasClass('active') == true) {
            var python_execution_form = $('#python_execution_form');
            if ($('textarea#python_textarea').val() == '') {
                show_toaster();
            } else {
                $.ajax({
                    url: '/python_execution/',
                    type: 'POST',
                    data: python_execution_form.serialize(),
                    success: function(res) {
                        if (res.hasOwnProperty('bar_chart')) {
                        	remove_charts_html();
                        	clear_python_op_texarea();
                            make_called_link_active('link_bar_chart');
                            draw_bar_chart(res.bar_chart);
                            show_python_op_block();

                        } else if (res.hasOwnProperty('line_chart')) {
                        	clear_python_op_texarea();
                        	remove_charts_html();
                            make_called_link_active('link_line_chart');
                            draw_line_chart(res.line_chart);
                            show_python_op_block();

                        } else if (res.hasOwnProperty('column_chart')) {
                        	clear_python_op_texarea();
                        	remove_charts_html();
                            make_called_link_active('link_column_chart');
                            draw_column_chart(res.column_chart);
                            show_python_op_block();

                        } else if(res.hasOwnProperty('pie_chart')){
                            clear_python_op_texarea();
                            remove_charts_html();
                            make_called_link_active('link_pie_chart');
                            draw_pie_chart(res.pie_chart);
                            show_python_op_block();


                        } else {
                        	remove_charts_html();
                            show_python_op_block();
                            clear_python_op_texarea();
                            $('#python_output').text(res);

                        }
                    },
                    error: function(res, jqXHR, textStatus, errorThrown) {
                    	remove_charts_html();
                        show_python_op_block();
                        clear_python_op_texarea();
                        $('#python_output').text(res.responseJSON.error);
                    }
                });
            }

        } else {
            var sql_execution_form = $('#sql_execution_form');
            if ($('textarea#sql_textarea').val() == '') {
               show_toaster();
            } else {
                $.ajax({
                    url: '/sql_execution/',
                    type: 'POST',
                    data: sql_execution_form.serialize(),
                    success: function(res) {
                        clear_python_op_texarea();
                        remove_charts_html();
                        draw_column_chart(res.chart_data);
                        draw_line_chart(res.chart_data);
                        make_called_link_active('link_column_chart');

                        show_sql_op_block();
                        $('#sql_execution_op_form').hide();
                        $('#sql_output_div_ajax').html(res.html);
                        $('#sql_output_div_ajax').show();
                    },
                    error: function(res, jqXHR, textStatus, errorThrown) {
                        clear_python_op_texarea();
                    	remove_charts_html();
                    	show_sql_op_block();
                    	$('#sql_output').text('');
                        $('#sql_output').text(res.responseJSON.error);
                        $('#sql_output_div_ajax').hide();
                        $('#sql_execution_op_form').show();
                    }
                })
            }
        }
    });

    /* Func to render vertical bars or column chart */
    function draw_column_chart(data){
        data.forEach(function(d) {
            var1 = Object.keys(d)[0]
            var2 = Object.keys(d)[1]
            d[var1] = d[var1]
            d[var2] = d[var2]
        });

        var margin = {
            top: 10,
            right: 60,
            bottom: 90,
            left: 60
        };

        var width = 500 - margin.left - margin.right;
        var height = 240 - margin.top - margin.bottom;


        var x = d3.scale.ordinal()
            .rangeRoundBands([0, width], .1);

        var y = d3.scale.linear()
            .range([height, 0]);

        var xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom");

        var yAxis = d3.svg.axis()
            .scale(y)
            .orient("left")
            // .tickFormat(formatPercent);

        var tip = d3.tip()
          .attr('class', 'd3-tip')
          .offset([-10, 0])
          .html(function(d) {
            return "<strong>"+d[var1]+":</strong> <span style='color:red'>" + d[var2] + "</span>";
          })

        svg = d3.select("#column_chart_svg").append('svg')
        .attr("width", '100%')
        .attr("height", '100%')
        .attr("viewBox", "0 0 " +(width + margin.left + margin.right) + " " + (height + margin.top + margin.bottom))
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        svg.call(tip);

        x.domain(data.map(function(d) { return d[var1] }));
        y.domain([0, d3.max(data, function(d) { return d[var2]; })]);

        svg.append("path")
        .datum(data)
        .attr("fill", "none")
        .attr("stroke", "steelblue");

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)
        .append("text")
            .attr("x", width / 2)
            .attr("y",  15)
            .attr("dy", "1em")
            .style("text-anchor", "middle")
            .style("font", "13px times")
            .text(var1);

        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis)
            .append("text")
              .attr("transform", "rotate(-90)")
              .attr("y", -35)
              .attr("dy", ".71em")
              .style("text-anchor", "end")
              .style("font", "13px times")
              .text(var2);

        svg.selectAll(".bar")
            .data(data)
            .enter().append("rect")
              .attr("class", "bar")
              .attr("x", function(d) { return x(d[var1]); })
              .attr("width", x.rangeBand())
              .attr("y", function(d) { return y(d[var2]); })
              .attr("height", function(d) { return height - y(d[var2]); })
              .on('mouseover', tip.show)
              .on('mouseout', tip.hide)
    }

    /* Func to render line chart */
    function draw_line_chart(data) {

        data.forEach(function(d) {
            var1 = Object.keys(d)[0]
            var2 = Object.keys(d)[1]
            d[var1] = d[var1]
            d[var2] = d[var2]
        });


        var margin = {
            top: 10,
            right: 20,
            bottom: 80,
            left: 50
        };

        var width = 420 - margin.left - margin.right;
        var height = 220 - margin.top - margin.bottom;

        var x = d3.scale.ordinal()
            .rangeRoundBands([0, width], .1);

        var y = d3.scale.linear()
            .range([height, 0]);

        var xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom");

        var yAxis = d3.svg.axis()
            .scale(y)
            .orient("left");

        var tip = d3.tip()
          .attr('class', 'd3-tip')
          .offset([-10, 0])
          .html(function(d) {
            return "<strong>"+d[var1]+":</strong> <span style='color:red'>" + d[var2] + "</span>";
          })

        var line = d3.svg.line()
            .x(function(d) { return x(d[var1]); })
            .y(function(d) { return y(d[var2]); });


        var svg = d3.select("#line_chart_svg").append('svg')
                .attr("width", '100%')
                .attr("height", '100%')
                .attr("viewBox", "0 0 " +(width + margin.left + margin.right) + " " + (height + margin.top + margin.bottom))
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");



        svg.call(tip);

        x.domain(data.map(function(d) { return d[var1] }));
        y.domain([0, d3.max(data, function(d) { return d[var2]; })]);

        // Add the X Axis
        svg.append("g")
              .attr("class", "x axis")
              .attr("transform", "translate(0," + height + ")")
              .call(xAxis)
            .append("text")
                .attr("x", width / 2)
                .attr("y",  20)
                .attr("dy", "1em")
                .style("text-anchor", "middle")
                .style("font", "13px times")
                .text(var1);

        // Add the Y Axis
        svg.append("g")
              .attr("class", "y axis")
              .call(yAxis)
            .append("text")
              .attr("transform", "rotate(-90)")
              .attr("y", -40)
              .attr("dy", ".71em")
              .style("text-anchor", "end")
              .style("font", "13px times")
              .text(var2);

        svg.append("path")
            .datum(data)
            .attr("class", "line")
            .attr("d", line);

        // Add the scatterplot
        svg.selectAll("dot")
            .data(data)
        .enter().append("circle")
            .attr("r", 4)
            .attr("cx", function(d) { return x(d[var1]); })
            .attr("cy", function(d) { return y(d[var2]); })
            .on('mouseover', tip.show)
            .on('mouseout', tip.hide);
    }

    /* Func to render Horizontal bars or column chart */
    function draw_bar_chart(data) {

        data.forEach(function(packet) {
            packet['color'] = getRandomColor();
        });


        var chart;
        nv.addGraph(function() {
            var margin = {
                top: 10,
                right: 10,
                bottom: 90,
                left: 60
            };
            var width = 900, height = 400;
            chart = nv.models.multiBarHorizontalChart()
                .x(function(d) { return d.label })
                .y(function(d) { return d.value })
                .width(width).height(height)
                .duration(500)
                .showControls(false)
                .showLegend(true)
                .legendPosition("top")
                .margin({left: 200, bottom:60})
                .stacked(false);
            chart.yAxis.axisLabel('Y Axis');
            chart.xAxis.axisLabel('X Axis').axisLabelDistance(100);
            d3.select('#bar_chart_svg').append('svg')
                .attr("width", '100%')
                .attr("height", '100%')
                .attr("viewBox", "0 0 " +(width + margin.left + margin.right) + " " + (height + margin.top + margin.bottom))
                .datum(data)
                .call(chart);

            return chart;
        });
    }

    /* Func to render pie chart  */
    function draw_pie_chart(data){

        var margin = {
            top: 10,
            right: 10,
            bottom: 110,
            left: 60
        };

        var width = 400,
        height = 400,
        outer_radius = 200,
        inner_radius = 80,
        color = d3.scale.category20c();

        var total = d3.sum(data, function(d) {
            return d3.sum(d3.values(d));
        });


        var vis = d3.select("#pie_chart_svg")
            .append("svg")
            .data([data])
            .attr("width", '100%')
            .attr("height", '100%')
            .attr("viewBox", "0 0 " +(width + margin.left + margin.right) + " " + (height + margin.top + margin.bottom))
            .append("g")
            .attr("transform", "translate(" + outer_radius * 1.1 + "," + outer_radius * 1.1 + ")")

        var div = d3.select("body").append("div").attr("class", "piechart_toolTip");

        var textTop = vis.append("text")
            .attr("dy", ".35em")
            .style("text-anchor", "middle")
            .attr("class", "textTop")
            .text( "TOTAL" )
            .attr("y", -10)
            .attr("x", -5),
        textMiddle = vis.append("text")
            .attr("dy", ".35em")
            .style("text-anchor", "middle")
            .attr("class", "textMiddle")
            .text('')
            .attr("y", 10);
        textBottom = vis.append("text")
            .attr("dy", ".35em")
            .style("text-anchor", "middle")
            .attr("class", "textBottom")
            .text(total.toFixed(2))
            .attr("y", 20);

        var arc = d3.svg.arc()
            .innerRadius(inner_radius)
            .outerRadius(outer_radius);

        var arcOver = d3.svg.arc()
            .innerRadius(inner_radius + 5)
            .outerRadius(outer_radius + 5);

        var pie = d3.layout.pie()
            .value(function(d) { return d.value; });

        var arcs = vis.selectAll("g.slice")
        .data(pie)
        .enter()
            .append("svg:g")
                .attr("class", "slice")
                .on("mouseover", function(d) {
                    d3.select(this).select("path").transition()
                        .duration(200)
                        .attr("d", arcOver)
                   div.style("left", d3.event.pageX+0+"px");
                      div.style("top", d3.event.pageY-0+"px");
                    div.style("display", "inline-block");
                    div.html((d.data.label)+"<br>"+(Math.round(1000* d.data.value/total ))/ 10 +"%");

                    textTop.text(d3.select(this).datum().data.label)
                        .attr("y", -10);
                    textMiddle.text(Math.round(1000 * d3.select(this).datum().data.value / total) / 10 + '%')
                        .attr("y", 20);
                    textBottom.text(d3.select(this).datum().data.value.toFixed(2))
                        .attr("y", 50);
                })
                .on("mouseout", function(d) {
                    d3.select(this).select("path").transition()
                        .duration(100)
                        .attr("d", arc);
                    div.style("display", "none");

                    textTop.text( "TOTAL" )
                        .attr("y", -10);
                    textMiddle.text('')
                    textBottom.text(total.toFixed(2)).attr("y", 20);
                });

        arcs.append("svg:path")
            .attr("fill", function(d, i) { return color(i); } )
            .attr("d", arc);
    }


    /* Func to make chart block tabs and tab's content class active  */
    function make_called_link_active(link_id) {
        var block_chart_ul = $("#block_chart_ul li");
        var chart_block_content_div = $("#chart_block_content_div");
        var href = '';
        /* loop over block_chart_ul a tag to make it active */
        block_chart_ul.each(function(idx, li) {
            if ($('a', li).attr('id') == link_id) {
                $('#' + link_id).addClass('active');
                href = $('a', li).attr('href');
            } else {
                $('#' + $('a', li).attr('id')).removeClass('active')
            }
        });

        /* loop over chart_block_content_div inner divs to make it active */
        $.each($('#chart_block_content_div  .tab-pane'), function(i, div) {
            if ($(div).attr('id') == href.substr(1)) {
                $(href).addClass('active');
            } else {
                $('#' + $(div).attr('id')).removeClass('active');
            }
        })
    }

    /* Func to remove html of charts to re-render it */
    function remove_charts_html(){
    	$('#bar_chart_svg').html('');
		$('#line_chart_svg').html('');
		$('#column_chart_svg').html('');
        $('#pie_chart_svg').html('');
    }

    /* Func to display pyhton execution block */
    function show_python_op_block(){

    	$('#link_exec_op').addClass('active');
        $('#execution_op').addClass('active');
        $('#link_table_result').removeClass('active');
        $('#table_op').removeClass('active');
        $('#sql_execution_op_form').hide();
    }

    /* Func to display sql execution block */
    function show_sql_op_block(){
    	$('#link_exec_op').removeClass('active')
        $('#execution_op').removeClass('active')
        $('#link_table_result').addClass('active')
        $('#table_op').addClass('active')
    }

    /* Random color generator for bar chart */
    function getRandomColor() {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++ ) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    /* func to display taoster when python and sql textarea is empty */
    function show_toaster(){
        toastr.error("Field can't be empty.", "Error", {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "2000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    }

    /* func to clear python output textarea */
    function clear_python_op_texarea(){
        $('#python_output').text('');
    }

// SAVE CREATION REPORTS IN DATABASE //
    var report_creation_form =$('#report_creation_form');

     $('#Save_data'). on('click', function(){
                var connection_name = $('#connection_name option').val();
                var connection_id   = $('#connection_id').val();
                var fields        = $('#fields').val();
                var group_by      = $('#group_by').val();
                var order_by      = $('#order_by').val();
                var where         = $('#where').val();
                var limit         = $('#limit').val();
                var mail_body     = $('#mail_body').val();
                var query_name    = $('#queryname').val();
                var subject       = $('#subject').val();
                var table         = $('#table').val();

             if((mail_body == "")||(query_name == "")||(subject == "")||(table == "")){

                 $('#error_mess').html('Please fill All (*)mendatory fields')
                 $('#error_mess').show().delay(2000).fadeOut();
             }
             else{
         $.ajax({
             url:'/save_report/',
             type:'POST',
             data:report_creation_form.serialize(),
             success: function(res){
             if(res=='success'){
                 $('#createreportmodal').modal('toggle');   
                 $('#success_message').show().delay(2000).fadeOut();
                 window.location.reload();
                 console.log('success')
            }
            else{
                 $('#error').show().delay(2000).fadeOut();
             }
         },
         error: function(jqXHR, textStatus, errorThrown){
             console.log(errorThrown)
         }
     })
     event.preventDefault();
     }
 });


// SAVE AND REDIRECT FROM REPORT TO ASSIGN REPORT TEMPLATE// 
 var report_creation_form = $('#report_creation_form');
  $('#save_countinue').on('click', function(){

             var connection_id = $('#connection_name option').val();
             var fields        = $('#fields').val();
             var group_by      = $('#group_by').val();
             var order_by      = $('#order_by').val();
             var where         = $('#where').val();
             var limit         = $('#limit').val();
             var mail_body     = $('#mail_body').val();
             var query_name    = $('#queryname').val();
             var subject       = $('#subject').val();
             var table         = $('#table').val();

          if((mail_body == "")||(query_name == "")||(subject == "")||(table == "")){
              $('#error_mess').html('Please fill All (*)mendatory fields')
              $('#error_mess').show().delay(2000).fadeOut();
          }
          else{
      $.ajax({
          url:'/report-creation/',
          type:'POST',
          data:report_creation_form.serialize(),
          success: function(res){
              if(res == 'success'){
              window.location.href='/assign_reports/'
              }
              else{
                  $('#error').show().delay(2000).fadeOut();
              }
           
           },
          error: function(jqXHR, textStatus, errorThrown) {
              console.log(errorThrown)
          }

      })
  event.preventDefault();
  }
});

//CREATE AN  ASSIGN REPORT DATA FROM ASSIGN-REPORTS//
var assign_report_form = $('#assign_report_form');

   $('#create').on('click', function(){

       // var source_id  = $('#src_id').val();
       var username   = $('#username').val();
       var email      = $('#email').val();
       var query_name = $('#query_name option:selected').text();
       var channel    = $('#channel').val();
       var condition  = $('#condition').val();

       if((username == "")||(email == "")||(query_name == "")){

            $("#error_message").show().delay(2000).fadeOut();
       }    
        else if(check_email($("#email").val())==false){
            $("#email_error_message").show().delay(2000).fadeOut();

            }

       else{
       $.ajax({
           url:'/add_report/',
           type:'POST',
           data:{  'username':username,
                   'email':email,
                   'query_name':query_name,
                   'channel': 'email',
                   'condition': condition
                },
           success: function(res) {
               if(res =='success')
                  
                    location.reload(true);


               
           },
           error: function(jqXHR, textStatus, errorThrown) {
               console.log(errorThrown)
           }

       })
   }
  });



/*DELETE A ROW FROM CREATION REPORTS DATATABLE*/
    $('.delete').click(function(){
        var report_id = $(this).attr('data-id')
        data={'report_id':report_id}
        if (confirm("Are you sure to delete this?")) {
            $.ajax({
                url:'/deleteCreationReport/',
                type:'POST',
                data:data,
                success: function(res){
                    if (res == 'success'){
                        window.location.reload();
                    }
                    // console.log('res',res)
                    // $('.assigned_reports_data').html(res)

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown)
                }

            });
        }
        return false;
    })



   
    /*DELETE ROW FROM ASSIGN REPORT DATATABLE*/
    $('.delete_row').click(function(){
        var report_id = $(this).attr('data-id')
        data={'report_id':report_id}
        if (confirm("Are you sure to delete this?")) {
            $.ajax({
                url:'/deleteAssignedReport/',
                type:'POST',
                data:data,
                success: function(res){
                    if (res == 'success'){
                        window.location.reload();
                    }
                    // console.log('res',res)
                    // $('.assigned_reports_data').html(res)

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown)
                }

            });
        }
        return false;
    })



 



    /*MODIFY ROW FROM ASSIGN REPORT DATATABLE*/
    $('.modify_row').click(function(){
        var report_id = $(this).attr('data-id')
        data={'report_id':report_id}
            $.ajax({
                url:'/getAssignedReportData/',
                type:'POST',
                data:data,
                success: function(res){
                    var report_data = res.report_data
                    $('#edit_id').val(report_data[0].id);
                    $('#edit_username').val(report_data[0].username);
                    $('#edit_email').val(report_data[0].email);
                    $('#edit_queryname').val(report_data[0].query_name);
                    $('#Modify_modal').modal('show');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown)
                }

            });
    })


     /*MODIFY ROW FROM CONNECTION DATATABLE*/
    $('.modify_connection').click(function(){
        var connection_id = $(this).attr('data-id')
        data={'connection_id':connection_id}
            $.ajax({
                url:'/getConnectionData/',
                type:'POST',
                data:data,
                success: function(res){
                    var connection_data = res.connection_data
                    $('#edit_id').val(connection_data[0].id);   
                    $('#edit_ipaddress').val(connection_data[0].ip_address);
                    $('#edit_port').val(connection_data[0].port);
                    if (connection_data[0].is_secure == 'true'){
                        $('#edit_issecure').attr('checked', 'checked');

                    }else{
                        $('#edit_issecure1').attr('checked', 'checked');
                    }
                    $('#edit_query_endpoint').val(connection_data[0].query_endpoint);
                    $('#edit_auth_endpoint').val(connection_data[0].auth_endpoint);
                    $('#edit_username').val(connection_data[0].username);
                    $('#edit_password').val(connection_data[0].password);
                    $('#edit_datasource_name').val(connection_data[0].data_source_name);
                    $('#edit_connection_name').val(connection_data[0].connection_name);

                    $('#Modify_modal').modal('show');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown)
                }

            });
    })


    /*UPDATE REPORTS FROM ASSIGN REPORT DATATABLE*/
    $('#update_report').click(function(){
        var report_id = $('#edit_id').val();
        var username= $('#edit_username').val();
        var email= $('#edit_email').val();
        var queryname= $('#edit_queryname option:selected').text();
        data={
            'report_id':report_id,
            'username':username,
            'email':email,
            'queryname':queryname

        }
        if(!username || !email || !queryname){
            $("#empty_message").show().delay(2000).fadeOut();

        }else{
            $.ajax({
                url:'/updateAssignedReport/',
                type:'POST',
                data:data,
                success: function(res){
                    if (res == 'success'){
                        window.location.reload();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown)
                }

            });
        }

    });



        // VALIDATION FOR PORT FIELD IN CONNECTION FORM//
        $('#edit_port').bind('keypress', function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#err_message").html("PORT must be in numbers only i.e. (80 or 8080)").show().delay(2000).fadeOut("slow");
               return false;

        }
    });

     


        // VALIDATION FOR IP FIELD IN CONNECTION FORM//
        $('#edit_ipaddress').bind('keypress', function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which > 54)) {
        //display error message
        $("#err_message").html("IP address should be in numbers only i.e(11.11.11.11)").show().delay(2000).fadeOut("slow");
               return false;
        }
   });

/*UPDATE A ROW FROM CONNECTION DATATABLE*/
    $('#Save_changes').click(function(){
        var connection_id = $('#edit_id').val();
        var data_source_name = $('#edit_datasource_name').val();
        var connection_name = $('#edit_connection_name').val();
        var ip = $('#edit_ipaddress').val();
        var port = $('#edit_port').val();
        var issecure = $("input[name='issecure']:checked").val();
        var query_endpoint = $('#edit_query_endpoint').val();
        var auth_endpoint = $('#edit_auth_endpoint').val();
        var username = $('#edit_username').val();
        var password = $('#edit_password').val();
       
       
        if((data_source_name == "")||(connection_name == "") || (ip == "") ||(port == "") ||(issecure == "") || (query_endpoint == "")|| (auth_endpoint == "")|| (username == "")|| (password == "")){
            
            $("#error_message_empty").show().delay(2000).fadeOut();
        
        }else if(check_ip($("#edit_ipaddress").val())==false){
                
                $("#err_message").html("invalid IP address.. Please enter valid IP address");
                $('#err_message').show().delay(2000).fadeOut();
            }
        else{
            $.ajax({
               url:'/updateConnectionData/',
                type:'POST',
                data:{
                    'connection_id': connection_id,
                    'datasource_name':data_source_name,
                    'connection_name':connection_name,
                    'ip':ip,
                    'port':port,
                    'issecure':issecure,
                    'query_endpoint':query_endpoint,
                    'auth_endpoint':auth_endpoint,
                    'username':username,
                    'password':password
                },
                success: function(res){
                    if (res == 'success'){
                        window.location.reload();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown)
                }

            })
        }

    });



// GETTING CREATION REPORT DATA TO ASSIGN REPORTS// 
$('.assignreports').click(function(){
        var report_id = $(this).attr('data-id')
        var query_name = $(this).attr('data-query')
        data={'report_id':report_id, 'query_name':query_name }
            $.ajax({
                url:'/getCreationReportData/',
                type:'POST',
                data:data,
                success: function(res){
                    var report_data = res.report_data
                    $('#src_id').val(report_data[0].id);
                    $('#query_name').val(report_data[0].query_name);
                    $('#addassign_modal').modal('show');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown)
                }

            });
    })




// ASSIGN REPORT FROM REPORT CREATION DATATABLE//
var assignreport_form = $('#assignreport_form');

   $('#add').on('click', function(){
       var source_id  = $('#src_id').val();
       var query_name = $('#query_name').val();
       var username   = $('#usrname').val();
       var email      = $('#email').val();
       var channel    = $('#channel').val();
       var condition  = $('#condition').val();

       if((username == "")||(email == "")){

            $("#errormessage").show().delay(2000).fadeOut();
       }    
        else if(check_email($("#email").val())==false){
            $("#email_error_message").show().delay(2000).fadeOut();

            }

       else{
       $.ajax({
           url:'/addassign_report/',
           type:'POST',
           data:{  'source_id':source_id,
                   'username':username,
                   'email':email,
                   'query_name':query_name,
                   'channel': 'email',
                   'condition': condition
                },
           success: function(res) {
               if(res =='success'){
                   $('#addassign_modal').modal('toggle');
                   $('#successmessage').show().delay(2000).fadeOut();
                }

             },
          
           error: function(jqXHR, textStatus, errorThrown) {
               console.log(errorThrown)
           }

       })
    }
  });

// EMAIL VALIDATION FUNCTION //
function check_email(email){
 var pattern = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);

 if(pattern.test(email)){
    
 return true;
 }
 else{
    
    return false;
  
 }
}


        // FOR RADIO BUTTON CLICKED SINGLE VALUE//
        $('input[type="radio"]').bind('click', function() { 
        if($(this).attr('class') == "radio_button"){ 
            $('input[type="radio"]').not(this).prop("checked", false); 
            } 
        }); 


        // FOR CHECKBOX CLICKED SINGLE VALUE//
        $('input[type="checkbox"]').bind('click', function() { 
        if($(this).attr('class') == "checkbox_button"){ 
            $('input[type="checkbox"]').not(this).prop("checked", false); 
            } 
        }); 




        // SHOW INPUT IN CREATE NEW RADIO BUTTON//

        $("input[name='create']").on('click',function() {
            if ($('#create_input').is(':checked')) {
                $('#show_dd').hide();
                $('#form_hidden').show();
                new_flag = true;
            } 
          });                

         // SHOW DROPDOWN IN EXISTING RADIO BUTTON//

        $("input[name='existing']").on('click',function() {
            if ($('#dropdown').is(':checked')) {
                // $('#show_input').hide();
                $('#form_hidden').hide();
                $('#show_dd').show();
                new_flag = false;
            } 
        });

               
        
        
        // VALIDATION FOR PORT FIELD IN CONNECTION FORM//
        $('#port').bind('keypress', function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#error_message").html("PORT must be in numbers only i.e. (80 or 8080)").show().delay(2000).fadeOut("slow");
               return false;

        }
    });

     


        // VALIDATION FOR IP FIELD IN CONNECTION FORM//
        $('#ip').bind('keypress', function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which > 54)) {
        //display error message
        $("#error_message").html("IP address should be in numbers only i.e(11.11.11.11)").show().delay(2000).fadeOut("slow");
               return false;
        }
   });

        
    // SUBMIT CONNECTION DATA FROM REPORT TEMPLATE//
    var connection_form = $('#Connection_form');

    $('#Create_con').on('click', function(){
            
            var datasourcename = $('#datasourcename').val();
            var connectionname = $('#connectionname').val();
            var ip = $('#ip').val();
            var port = $('#port').val();
            var issecure = $("input[name='issecure']:checked").val();
            var queryendpoint = $('#query_endpoint').val();
            var authendpoint = $('#auth_endpoint').val();
            var username = $('#username').val();
            var password = $('#password').val();

        
            if((datasourcename == "")||(connectionname == "")||(ip == "")||(port == "")||(issecure == "")||(queryendpoint == "")||(authendpoint == "")||(username == "")||(password == "")){

                $('#error_message').html('Please Fill Empty Fields');
                $('#error_message').show().delay(2000).fadeOut();
            }


            else if(check_ip($("#ip").val())==false){
                
                $("#error_message").html("invalid IP address.. Please enter valid IP address");
                $('#error_message').show().delay(2000).fadeOut();
            }
              
            else{

        $.ajax({
           url:'/saveConnectionsData/',
           type:'POST',
           data: {
                   'datasourcename':datasourcename,
                   'connectionname':connectionname,
                   'ip': ip,
                   'port': port,
                   'issecure': issecure,
                   'queryendpoint': queryendpoint,
                   'authendpoint': authendpoint,
                   'username':username,
                   'password':password,
                   
                }, 
           success: function(res) {
                if(res =='success'){
                   $('#success_message').show().delay(2000).fadeOut();
                } else if(res == 'already existing datasource name'){
                   $('#error_message').html('Datasource name already exist.').show().delay(2000).fadeOut();
                } else if(res == 'already existing connection name'){
                    $('#error_message1').html('Connection name already exist.').show().delay(2000).fadeOut();
                }
               
                
                

             },
          
           error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus)
           }

       })
    }
});
// IP VALIDATION FUNCTION//
function check_ip(ip){
var ipformat = new RegExp(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/);
if(ipformat.test(ip))
 {
    return true;
}
  else{  
    return false;
    }
}



/*DELETE A ROW FROM CONNECTION DATA*/
$('.delete_connection').click(function(){
    var connection_id = $(this).attr('data-id')
    data={'connection_id':connection_id}
    if (confirm("Are you sure to delete this?")) {
        $.ajax({
            url:'/deleteConnectionData/',
            type:'POST',
            data:data,
            success: function(res){
                if (res == 'success'){
                    window.location.reload();
                }
               

            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown)
            }

        });
    }
    return false;
});


// GETTING ASSIGNED REPORT ID//
$(".Schedule_button").click(function(){
        var report_id = $(this).attr('data-id')
        data={'report_id':report_id}
            $.ajax({
                url:'/getAssignedReportData/',
                type:'POST',
                data:data,
                success: function(res){
                    var report_data = res.report_data
                    $("#id").val(report_data[0].id);
                     },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown)
                }

            });
    });

// SAVING DATA FRONTEND TO BACKEND//
$('#dag_scheduler').on('click',function(){
    var report_id   = $("#id").val();
    var date = $('#date').val();
    var schedule_time = $('#schedule-val').text();
    var timezone = $('#Timezone').val();
        if((date =="")||(schedule_time == "")||(timezone == "" )){
          $('#empty_error_message').show().delay(2000).fadeOut();  
        }
        else{
        $.ajax({
                    url:'/dagScheduler/',
                    type:'POST',
                    data:{'report_id':report_id,
                          'time': schedule_time,
                           'date': date,
                           'timezone':timezone
                       },     
                    success: function(res){
                        if(res == 'success'){
                            $('#Scheduler-modal').modal('toggle');
                            $('#report_success_message').show().delay(2000).fadeOut();  

                        }

                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(errorThrown)
                    }

              
              })
            return false;
        }
    });

// DROPDOWN TO SELECT START DATE IN SCHEDULER MODAL// 
$(function(){
    $('#date').combodate({
    firstItem: 'name', 
    format: 'YYYY-MM-DD',
    template:'YYYY MM D',   
    customClass: 'form-control'
    });    
});


// SCHEDULE INTERVAL TIMING CRONJOB EDITOR //
$("#schedule").cron({
    onChange: function() {    
    $("#schedule-val").text($(this).cron("value"));
    },
    useGentleSelect: true // default: false
    });


// TIMEZONE LIST SCHEDULER MODAL//
$('#Timezone').timezones();
});



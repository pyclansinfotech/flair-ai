$( function(){

    if(window.location.pathname == "/manager/fileupload/"){
        $("#upload_step").removeClass('ap-step-inactive');
        $("#upload_step").removeClass('ap-step-disabled');
        $("#upload_step").addClass('ap-step-active');
        $("#profiling_step").removeClass('ap-step-active');
        $("#profiling_step").removeClass('ap-step-inactive');
        $("#profiling_step").addClass('ap-step-disabled');
        $("#finish_step").removeClass('ap-step-active');
        $("#finish_step").removeClass('ap-step-inactive');
        $("#finish_step").addClass('ap-step-disabled');
    }else if(window.location.pathname.includes("/manager/profiling/")){
        $("#upload_step").removeClass('ap-step-active');
        $("#upload_step").removeClass('ap-step-disabled');
        $("#upload_step").addClass('ap-step-inactive');
        $("#profiling_step").removeClass('ap-step-disable');
        $("#profiling_step").removeClass('ap-step-inactive');
        $("#profiling_step").addClass('ap-step-active');
        $("#finish_step").removeClass('ap-step-active');
        $("#finish_step").removeClass('ap-step-inactive');
        $("#finish_step").addClass('ap-step-disabled');
    }else if(window.location.pathname.includes("/manager/finish/")){
        $("#upload_step").removeClass('ap-step-active');
        $("#upload_step").removeClass('ap-step-disabled');
        $("#upload_step").addClass('ap-step-inactive');
        $("#profiling_step").removeClass('ap-step-disable');
        $("#profiling_step").removeClass('ap-step-active');
        $("#profiling_step").addClass('ap-step-inactive');
        $("#finish_step").removeClass('ap-step-disable');
        $("#finish_step").removeClass('ap-step-inactive');
        $("#finish_step").addClass('ap-step-active');
    }

});
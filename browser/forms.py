from django import forms
from .models import Source
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field

class SourceForm(forms.Form):
    table = forms.CharField(max_length=250)
    docfile = forms.FileField()





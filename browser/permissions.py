from rest_framework import permissions


class IsTopOwner(permissions.BasePermission):
    def has_permission(self, request, view):
        user = request.user
        # check if the user has required permission based on request url
        if 'view/feature' in request.path:
            # decide which perm to check
            if user.has_perm('browser.read_access_feature'):
                return True
        elif 'view/source' in request.path:
            if user.has_perm('browser.read_access_source'):
                return True
        return False


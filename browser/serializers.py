from .models import Source, Feature, FieldType, DataType
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'username',
            'is_active',
            'is_superuser',
        )

class SourceSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    class Meta:
        model = Source
        fields = (
            'id',
            'table',
            'docfile',
            'file_location',
            'ods_file_location',
            'identifier',
            'in_memory',
            'analysed',
            'extension',
            'user',
        )

class SourceViewerSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    class Meta:
        model = Source
        fields = (
            'id',
            'table',
            'docfile',
            'file_location',
            'ods_file_location',
            'identifier',
            'in_memory',
            'analysed',
            'extension',
            'user',
        )

class FieldTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = FieldType
        fields = (
            'id',
            'field_type'
        )

class DataTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = DataType
        fields = (
            'id',
            'data_type',
        )

    def create(self, validated_data):
        datatype = DataType(
            data_type=validated_data['data_type']
        )
        datatype.save()
        return datatype

    def update(self, instance, validated_data):
        instance.data_type = validated_data.get('data_type', instance.data_type)
        instance.save()
        return instance

    def delete(self, request, pk, format=None):
        datatype = self.get_object(pk)
        datatype.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class FeatureSerializer(serializers.ModelSerializer):

    class Meta:
        model = Feature
        fields = (
            'id',
            'field_name',
            'density',
            'source_link',
            'fieldtype_link',
            'datatype_link',
        )

class FeatureViewerSerializer(serializers.ModelSerializer):
    source_link = SourceViewerSerializer()
    fieldtype_link = FieldTypeSerializer()
    datatype_link = DataTypeSerializer()

    class Meta:
        model = Feature
        fields = (
            'id',
            'field_name',
            'density',
            'source_link',
            'fieldtype_link',
            'datatype_link',
        )

from rest_framework import routers
from browser.views import SourceViewSet, FeatureViewSet, FieldTypeViewSet, DataTypeViewSet, FeatureViewOnlySet, SourceViewOnlySet


class BrowserRouterRegistry(object):

    @classmethod
    def buildrouters(self):
        router = routers.DefaultRouter()
        router.register(r'^api/v1/source', SourceViewSet)
        router.register(r'^api/v1/feature', FeatureViewSet)
        router.register(r'^api/v1/datatype', DataTypeViewSet)
        router.register(r'^api/v1/fieldtype', FieldTypeViewSet)
        router.register(r'api/v1/view/feature', FeatureViewOnlySet)
        router.register(r'api/v1/view/source', SourceViewOnlySet)
        return router

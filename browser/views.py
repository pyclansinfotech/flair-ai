from rest_framework import viewsets
from .serializers import *
from rest_framework import permissions
from .permissions import IsTopOwner
from django.shortcuts import render
from django.contrib.auth.views import login
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponse, Http404, JsonResponse
from .forms import SourceForm
import uuid
from django.shortcuts import reverse
from Profiler.analyse import DataProfiler
import os
from utils.summary import SourceSummary, Sources
from ai.settings import BASE_DIR
from sparkplug.parquet import SparkPlug
from scrapbook.inkme import Ink
from utils.actlog import Activitylogger
from django.template.loader import render_to_string
from django.template import Context, Template, RequestContext
from utils.feeder import SourceFeed


class SourceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Sources to be viewed or edited.
    """
    Ink.info("SourceViewSet registered as an endpoint")
    queryset = Source.objects.all()
    serializer_class = SourceSerializer
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)


class SourceViewOnlySet(viewsets.ModelViewSet):
    """
    API endpoint that allows Features to be viewed or edited.
    """
    Ink.info("SourceViewOnlySet registered as an endpoint")
    queryset = Source.objects.all()
    serializer_class = SourceViewerSerializer
    permission_classes = (permissions.IsAuthenticated, IsTopOwner,)
    http_method_names = ['get']


class FeatureViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Features to be viewed or edited.
    """
    Ink.info("FeatureViewSet registered as an endpoint")
    queryset = Feature.objects.all()
    serializer_class = FeatureSerializer
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)


class FeatureViewOnlySet(viewsets.ModelViewSet):
    """
    API endpoint that allows Features to be viewed or edited.
    """
    Ink.info("FeatureViewOnlySet registered as an endpoint")
    queryset = Feature.objects.all()
    serializer_class = FeatureViewerSerializer
    permission_classes = (permissions.IsAuthenticated, IsTopOwner,)
    http_method_names = ['get']


class FieldTypeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows FieldTypes to be viewed or edited.
    """
    Ink.info("FieldTypeViewSet registered as an endpoint")
    queryset = FieldType.objects.all()
    serializer_class = FieldTypeSerializer
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)


class DataTypeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows DataTypes to be viewed or edited.
    """
    Ink.info("DataTypeViewSet registered as an endpoint")
    queryset = DataType.objects.all()
    serializer_class = DataTypeSerializer
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)


def index(request):
    Ink.info("Index requested")
    return render(request, "index.html")


def checkuser(request):
    Ink.info("checkuser requested")
    if request.user.is_authenticated():
        return True
    else:
        return redirect('/admin/')


@login_required()
def file_upload(request):
    if request.method == 'POST':
        form = SourceForm(request.POST, request.FILES)
        if form.is_valid():
            cd = form.cleaned_data
            newSource = Source(docfile=request.FILES.get('docfile', False))
            newSource.id = uuid.uuid1()
            newSource.table = cd['table']
            newSource.identifier = uuid.uuid1()
            newSource.ods_file_location = ""
            newSource.in_memory = False
            newSource.analysed = False
            newSource.file_location, newSource.extension = os.path.splitext(str(request.FILES.get('docfile', False)))
            newSource.user = User.objects.get(username=request.user)
            newSource.save()
            Activitylogger.write({
                "type": "INFO",
                "message": "New source available : {0}".format(cd['table']),
                "display": True,
                "styles": "",
                "icon": "fa fa-home",
                "username": request.user
            })
            return JsonResponse({'id':newSource.id}, status=200)
        else:
            return Http404
    else:
        return render(request, 'browser/file-upload.html')


@login_required()
def profiling(request, id):
    if request.method == 'POST':
        metadata = {}
        metadata['source'] = Source.objects.get(id=request.POST.get('source'))
        fields = request.POST.getlist('field')
        metadata['fields'] = []
        for field in fields:

            if not Feature.objects.filter(source_link=metadata['source'], field_name=field).exists():
                field_props = Feature(id=uuid.uuid1())
            else:
                field_props = Feature.objects.get(id=request.POST.get('u!'+field))
            field_props.field_name = field
            field_props.density = 0
            field_props.source_link = metadata['source']
            field_props.fieldtype_link = FieldType.objects.get(field_type=request.POST.get('f!' + field))
            field_props.datatype_link = DataType.objects.get(data_type=request.POST.get('d!' + field))
            field_props.save()
        Activitylogger.write({
            "type": "INFO",
            "message": "There were {0} features identified and added. {1} are all saved sucessfully".format(len(fields), str(fields)),
            "display": True,
            "styles": "",
            "icon": "fa fa-home",
            "username": request.user
        })
        return JsonResponse({'id':id}, status=200)
    else:
        profile = DataProfiler(id)
        data = profile.analyse()
        html =  render_to_string('browser/profiling.html', {"data": data}, request=request)
        return HttpResponse(html)


@login_required()
def finish(request, id):
    if request.method == 'POST':
        return redirect(reverse('sources'))
    else:

        SS = SourceSummary(id)
        data = SS.profile()
        html =  render_to_string('browser/finish.html', {"data": data}, request=request)
        return HttpResponse(html)


@login_required()
def sources(request):
    if request.method == 'POST':
        return redirect(reverse('home'))
    else:
        S = Sources.getall()
        totalsources = SourceFeed.totsources()
        sinmemory = SourceFeed.totsourcesinmemory()
        return render(request, 'browser/sources.html', {"data": S, 'totalsources':totalsources, 'sinmemory':sinmemory})



@login_required()
def deletesource(request, id):
    if request.method == 'POST':
        source = Source.objects.get(id=id)
        name = source.table
        filelocation = BASE_DIR + '/' + str(source.docfile)
        features = Feature.objects.filter(source_link=source)
        features.delete()
        os.remove(filelocation)
        source.delete()
        Activitylogger.write({
            "type": "INFO",
            "message": "{0} datasource has been deleted sucessfully".format(name),
            "display": True,
            "styles": "",
            "icon": "fa fa-home",
            "username": request.user
        })
        return redirect(reverse('sources'))

@login_required()
def conversion(request):
    if request.method == 'POST':
        return redirect(reverse('home'))
    else:
        S = Sources.getall()
        totalsources = SourceFeed.totsources()
        sinmemory = SourceFeed.totsourcesinmemory()
        return render(request, 'browser/conversion.html', {"data": S, 'totalsources':totalsources, 'sinmemory':sinmemory})

@login_required()
def runconversion(request, id):
    if request.method == 'POST':
        response = SparkPlug.createparquet(id)
        source = Source.objects.get(id=id)
        Activitylogger.write({
            "type": "INFO",
            "message": "{0} datasource has been optimized".format(source.table),
            "display": True,
            "styles": "",
            "icon": "fa fa-home",
            "username": request.user
        })
        return redirect(reverse('conversion'))


@login_required()
def pushtomemory(request, id):
    if request.method == 'POST':
        source = Source.objects.get(id=id)
        try:
            if request.POST.get("i_"+id) == 'on':
                if SparkPlug.memorypush( id ) is True:
                    source.in_memory = True
                    Activitylogger.write({
                        "type": "INFO",
                        "message": "{0} datasource has been cached".format(source.table),
                        "display": True,
                        "styles": "",
                        "icon": "fa fa-home",
                        "username": request.user
                    })
            else:
                if SparkPlug.flushmemory( id ) is True:
                    source.in_memory = False
                    Activitylogger.write({
                        "type": "INFO",
                        "message": "{0} datasource has been droped from cached".format(source.table),
                        "display": True,
                        "styles": "",
                        "icon": "fa fa-home",
                        "username": request.user
                    })
            source.save()
            return redirect(reverse('conversion'))
        except Exception as e:
            Ink.error("Flushind {0} from memory failed >> \n {1}".format(id, e))
            Activitylogger.write({
                "type": "ERROR",
                "message": "Error flushing data from cache for {0} datasource".format(id),
                "display": True,
                "styles": "",
                "icon": "fa fa-home",
                "username": request.user
            })
            return redirect(reverse('conversion'))
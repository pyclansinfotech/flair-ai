from django.db import models
import uuid
from django.contrib.auth.models import User


def generate_filename(instance, filename):
    print(str(instance))
    url = "data/users/{0}/{1}".format(instance.user.username, filename)
    return url


class Source(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    table = models.CharField(max_length=250)
    docfile = models.FileField(upload_to=generate_filename, null=True)
    file_location = models.TextField(null=True)
    ods_file_location = models.TextField(null=True)
    identifier = models.UUIDField(default=uuid.uuid1())
    in_memory = models.BooleanField(default=False)
    analysed = models.BooleanField(default=False)
    extension = models.CharField(max_length=10)
    user = models.ForeignKey(User)

    class Meta:
        permissions = (
            ("read_access_source", "Read Access to Source"),
        )

    def __str__(self):
        return self.table


class FieldType(models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    field_type = models.CharField(max_length=50)

    def __str__(self):
        return self.field_type


class DataType(models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    data_type = models.CharField(max_length=50)

    def __str__(self):
        return self.data_type


class Feature(models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    field_name = models.CharField(max_length=100)
    density = models.DecimalField(decimal_places=2, max_digits=5)
    source_link = models.ForeignKey(Source, on_delete=models.CASCADE)
    fieldtype_link = models.ForeignKey(FieldType)
    datatype_link = models.ForeignKey(DataType)

    class Meta:
        permissions = (
            ("read_access_feature", "Read Access to feature"),
        )

    def __str__(self):
        return "{0} {1}".format(self.source, self.field_name)

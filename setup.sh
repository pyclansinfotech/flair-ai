#!/usr/bin/env bash

pip3 install -r requirements.txt

export PYSPARK_PYTHON=/Library/Frameworks/Python.framework/Versions/3.6/bin/python3.6

export PYSPARK_DRIVER_PYTHON=/Library/Frameworks/Python.framework/Versions/3.6/bin/python3.6

python3 manage.py makemigrations

python3 manage.py migrate

#python3 manage.py collectstatic

#echo "from django.contrib.auth.models import User; User.objects.create_superuser('flairai', 'admin@flair.com', 'machineslearn')" | python3 manage.py shell

chmod 700 init.sh

./init.sh

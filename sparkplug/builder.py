from .stroke import Stroke
from browser.models import Source
from rest_framework import status
from .constants import Constants


class Query(object):

    @classmethod
    def execute(cls, params):
        response = {'data': [], 'status': status.HTTP_404_NOT_FOUND}
        try:
            source = Source.objects.values('in_memory', 'ods_file_location', 'table').get(table=params['table'][0])
            if source['in_memory'] is True:
                query = cls.build(cls, params, source['table'])
                torque = Stroke.execute(source['table'], query)
                response['status'] = status.HTTP_200_OK
            elif cls.odsFileExists(cls, source['ods_file_location']):
                query = cls.build(cls, params, 'parquet.`' + source['ods_file_location'] + '`')
                torque = Stroke.execute(source['table'], query)
                response['status'] = status.HTTP_200_OK
            else:
                torque = [
                    {
                        "message": "The requested source exists, but not optimised for querying."
                                   " Please contact your system administrator for more information."
                     }
                          ]
                response['status'] = status.HTTP_302_FOUND
        except Exception as e:
            torque = [
                {
                    "message": "The requested source does not exist or not optimised for querying."
                                 " Please contact your system administrator for more information."
                 }
            ]

        response['data'] = torque
        return response

    def odsFileExists(self, file_location):
        return len(file_location) > 2

    def build(self, params, table):
            queryStrings = []
            queryStrings.append(Constants.SELECT_UPPERCASE)
            queryStrings.append(Constants.COMMA_SEPERATOR.join(params[Constants.FIELDS_LOWERCASE]))
            queryStrings.append(Constants.FROM_UPPERCASE)
            queryStrings.append(table)
            if self.checkExistsAndLengthGreaterThanZero(self, Constants.WHERE_LOWERCASE, params):
                queryStrings.append(Constants.WHERE_UPPERCASE)
                queryStrings.append(Constants.AND_UPPERCASE.join(params[Constants.WHERE_LOWERCASE]))
            if self.checkExistsAndLengthGreaterThanZero(self, Constants.GROUPBY_LOWERCASE, params):
                queryStrings.append(Constants.GROUPBY_UPPERCASE)
                queryStrings.append(Constants.COMMA_SEPERATOR.join(params[Constants.GROUPBY_LOWERCASE]))
            if self.checkExistsAndLengthGreaterThanZero(self, Constants.ORDERBY_LOWERCASE, params):
                queryStrings.append(Constants.ORDERBY_UPPERCASE)
                queryStrings.append(Constants.COMMA_SEPERATOR.join(params[Constants.ORDERBY_LOWERCASE]))
            if self.checkExistsAndLengthGreaterThanZero(self, Constants.LIMIT_LOWERCASE, params):
                queryStrings.append(Constants.LIMIT_UPPERCASE)
                queryStrings.append(params[Constants.LIMIT_LOWERCASE][0])
            return " ".join(queryStrings)

    def checkExistsAndLengthGreaterThanZero(self, key, params):
        try:
            return len(params[key]) > 0
        except Exception:
            return False
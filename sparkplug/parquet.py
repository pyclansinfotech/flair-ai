from ai.settings import BASE_DIR
from browser.models import Source
import os, shutil
from scrapbook.inkme import Ink
from .kickstart import SparkContext
import pandas as pd

class SparkPlug(object):

    @classmethod
    def createparquet(cls, uid):
        Ink.debug("Creating parquet for >> {0}".format(uid))
        cls.spark = SparkContext.provide(uid)
        source = Source.objects.get(id=uid)
        fileloaction = BASE_DIR + '/' + str(source.docfile)
        desiredfileloc = BASE_DIR + '/data/users/' + source.user.username + '/' + source.table + '.parquet'
        if source.extension == '.json':
            try:
                df = cls.spark.read.load(fileloaction, format="json")
                if os.path.isdir(desiredfileloc):
                    shutil.rmtree(desiredfileloc)
                df.write.save(desiredfileloc, format="parquet")
                source.ods_file_location = desiredfileloc
                source.save()
                Ink.debug("Parquet created and saved in {0} for {1}".format(desiredfileloc, uid))
                return {"outcome": True, "status_text": "OK"}
            except Exception as e:
                Ink.error("Error in processing >> {0} >> \n {1}".format(uid, e))
                return {"outcome": False, "status_text": "Issue converting parquet"}
        elif source.extension == '.csv':
            try:
                loaded_content = pd.read_csv(fileloaction)
                df = cls.spark.createDataFrame(loaded_content)
                if os.path.isdir(desiredfileloc):
                    shutil.rmtree(desiredfileloc)
                df.write.save(desiredfileloc, format="parquet")
                source.ods_file_location = desiredfileloc
                source.save()
                Ink.debug("Parquet created and saved in {0} for {1}".format(desiredfileloc, uid))
                return {"outcome": True, "status_text": "OK"}
            except Exception as e:
                Ink.error("Error in processing >> {0} >> \n {1}".format(uid, e))
                return {"outcome": False, "status_text": "Issue converting parquet"}
        else:
            Ink.error("Error in processing >> {0} >> {1}".format(uid, "Format incompatibiity"))
            return {"outcome": False, "status_text": "Format incompatibiity"}

    @classmethod
    def memorypush(cls, uid):
        Ink.debug("Pushing data to memory >> {0}".format(uid))
        cls.spark = SparkContext.provide(uid)
        source = Source.objects.get(id=uid)
        try:
            if len(source.ods_file_location) > 2:
                df = cls.spark.read.parquet(source.ods_file_location)
            else:
                if source.extension == '.json':
                    df = cls.spark.read.load(BASE_DIR + '/' + str(source.docfile), format="json")
                if source.extension == '.csv':
                    loaded_content = pd.read_csv(BASE_DIR + '/' + str(source.docfile))
                    df = cls.spark.createDataFrame(loaded_content)
            df.createOrReplaceTempView(source.table)
            cls.spark.catalog.cacheTable(source.table)
            Ink.debug("Data pushed to memory >> {0} >> \n {1}".format(uid, df.printSchema()))
            alltables = cls.spark.sql("show tables")
            alltables.show()
            return True
        except Exception as e:
            Ink.error("Error in processing >> {0} >> {1}".format(uid, e))
            return False

    @classmethod
    def flushmemory(cls, uid):
        Ink.debug("Flusing memory for {0}".format(uid))
        cls.spark = SparkContext.provide(uid)
        source = Source.objects.get(id=uid)
        try:
            cls.spark.catalog.uncacheTable(source.table)
            cls.spark.catalog.dropTempView(source.table)
            Ink.debug("Memory flushed for {0}".format(uid))
            alltables = cls.spark.sql("show tables")
            alltables.show()
            return True
        except Exception as e:
            Ink.error("Error in processing >> {0} >> {1}".format(uid, e))
            return False

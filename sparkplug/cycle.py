from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .builder import Query

@api_view(['POST'])
def fire(request, format=None):
    boost = Query.execute(request.data['queryDTO'])
    return Response(boost['data'], status=boost['status'])
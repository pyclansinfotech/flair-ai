from pyspark.sql import SparkSession
from ai.settings import BASE_DIR
from browser.models import Source
from scrapbook.inkme import Ink

class SparkContext(object):

    @classmethod
    def provide(self, uid):
        try:
            Ink.debug("Initializing/Retriving spark session for >> {0}".format(uid))
            self.id = uid
            self.source = Source.objects.get(id=uid)
            self.spark = SparkSession \
                .builder \
                .appName(self.source.table) \
                .config("spark.driver.host", "localhost") \
                .config("spark.sql.inMemoryColumnarStorage.compressed", "true") \
                .getOrCreate()
            Ink.debug("Spark Session created/retrived for >> {0}".format(uid))
            return self.spark
        except Exception as e:
            Ink.critical("Error in initializing/retriving spark session >> {0} >> {1}".format(uid, e))

    @classmethod
    def read(self, table):
        try:
            Ink.debug("Initializing/Retriving spark session for >> {0}".format(table))
            self.spark = SparkSession \
                .builder \
                .appName(table) \
                .config("spark.driver.host", "localhost") \
                .config("spark.sql.inMemoryColumnarStorage.compressed", "true") \
                .getOrCreate()
            Ink.debug("Spark Session created/retrived for >> {0}".format(table))
            return self.spark
        except Exception as e:
            Ink.critical("Error in initializing/retriving spark session >> {0} >> {1}".format(table, e))

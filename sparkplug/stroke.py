from .kickstart import SparkContext


class Stroke(object):
    @classmethod
    def execute(cls, table, query):
        print(query)
        cls.spark = SparkContext.read(table)
        results = cls.spark.sql(query)
        results.show()
        columns = results.columns
        info = results.collect()
        data = []
        line = {}
        count = 0
        for row in info:
            for field in columns:
                line[field] = row[field]
            data.append(line)
            line = {}
            count = count + 1
        return data

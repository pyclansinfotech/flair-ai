import os
import sys

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ai.settings")
sys.path.append(os.path.abspath(os.path.join(BASE_DIR, os.pardir)))

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

import airflow
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
from home.models import ReportCreationData, AssignReportsData, Connections, \
    AirflowTasks
from send_emails import dag_call
import subprocess


task_obj = AirflowTasks.objects.all()
if task_obj:
    for obj in task_obj:
        if obj.id:
            start_date = obj.start_date - timedelta(days=1)
            start_date = datetime.strptime('%s %s %s' % (
                start_date.year,
                start_date.month,
                start_date.day), '%Y %m %d')
            args = {
                'owner': 'airflow',
                'depends_on_past': False,
                'start_date': start_date,
            }
            dag_id = 'send_report_{}'.format(obj.id)
            dag = DAG(
                dag_id,
                default_args=args,
                schedule_interval=obj.schedule_interval
            )
            globals()[dag_id] = dag
            task_id = 'task_'.format(obj.id)
            task_name = 'send_report_{}'.format(obj.id)
            task_id = PythonOperator(
                dag=dag,
                task_id=task_name,
                provide_context=True,
                python_callable=dag_call,
                op_kwargs={'report_id': obj.assign_task_id}
            )

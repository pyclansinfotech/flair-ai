import logging, os, datetime


class Ink(object):

    bd = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    logger = logging.getLogger('flair-ai')
    logger.setLevel(logging.DEBUG)
    filelocation = bd + '/logs/' + datetime.datetime.today().strftime('%Y%m%d') + '_scrapbook_'\
                   + str(datetime.datetime.now().hour) + '.log'
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    fh = logging.FileHandler(filelocation, mode='w')
    fh.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add formatter to ch
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)
    logger.addHandler(fh)

    @classmethod
    def debug(cls, message):
        cls.logger.debug(message)

    @classmethod
    def info(cls, message):
        cls.logger.info(message)

    @classmethod
    def warning(cls, message):
        cls.logger.warning(message)

    @classmethod
    def error(cls, message):
        cls.logger.error(message)

    @classmethod
    def critical(cls, message):
        cls.logger.critical(message)

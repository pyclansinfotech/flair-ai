# Flair Analytics Platform

## Scope of this project

  - Should be able to import csv and json files
  - Optimize data storage (Compression)
  - Enable SQL execution on flatfiles
  - Store data in memory using Spark
  - Control access/permissions
  - RESTfull interface to gt talking with the layer
  - Run Predictive and machine learning algorithums on result sets
  - Analyse the datasets and return prediction results
  - Should be able to create custom functions in Python
  - Event based reporting via Email and Push notifications
  - More to come......

### Tech

Core Technologies Used:
* Python3.6 with pip and 
* Postgres Database acting as a metastore

#### Dependencies
This project is built using python and the modules used in it are:

* Psycopg2==2.7.3.1
* Django==1.11.5
* djangorestframework==3.5.3
* djangorestframework-jwt==1.11.0
* py4j==0.10.4
* pyspark==2.2.1
* pandas==0.19.2
* django-crispy-forms==1.6.1
* cryptography==2.1.4
* airflow==1.8.0
* pdfkit==0.6.1
* requests==2.18.4
* wkhtmltopdf==0.2


Note: All of these dependencies are mentioned in the `requirements.txt` file


### Installation

"To installl everything usin a script"
rund python3 project_setup.py


"other way to run"
Pre-requesites to run the installation script:
* Install postgres
* Install Scala
* Install Python3.6
* Install pip
- Create a database in postgres names `flairap`
```sh
$ su - postgres
$ psql -U postgres
$ postgres#~ create database flairap;
```

* Make sure you have installed python and `cd` into the root directory of the application

```sh
$ cd your/path/flair-ai
$ ./setup.sh
$ python manage.py runserver
```
> Note:
> Since these project is built on a mac the default python keyword used is `python3`. If its different in your case please replace the python3 keyword to its equivalent.

- This would install all the necessary dependencies as listed in the requirements.txt file

- You can now see the application running on port `8000`


### Todos
 - Build GUI for custom querying in python and SQL
 - Visualizations for data
 - Run Predictive analysis and Machine learning on the datasets
 - Create custom functions
 - Event based reporting

#Airflow Configuration
1-Create the AIRFLOW_HOME directory in main directory(i.e. flair-ai).
$ mkdir airflow_home
$ export AIRFLOW_HOME=`pwd`/airflow_home
$ airflow version

Note: Above command will create default configuraion file.

2-Configure airflow with postgres by editing default airflow config file(airflow.cgf)
a)Replace default sql_alchemy_conn url to postgres url
sql_alchemy_conn = postgresql+psycopg2://username:password@localhost/airflow

b) Change dags_are_paused_at_creation value to False
dags_are_paused_at_creation = False

3)Initialize the Airflow DB
$ airflow initdb

4)Create dags folder inside airflow_home directory
$ mkdir airflow_home/dags

5) Create scheduler file(contains scheduler code)
$ touch airflow_home/dags/scheduler.py

6) To start airflow webserver
$ airflow webserver

7) To run airflow scheduler(Use separate terminal and make sure virtual environment is activated )
$ export AIRFLOW_HOME=`pwd`/airflow_home
$ airflow scheduler
